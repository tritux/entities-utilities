package com.tritux.common.entities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * Generates persistent entity classes from an XML source
 * @author Tritux
 * @goal generate-sources
 * @phase generate
 */
public class CodeGeneratorMojo extends AbstractMojo {

	private static final int N_1000 = 1000;

	/**
	 * @parameter alias="generate.location"
	 * @required
	 */
	private String destination;

	/**
	 * @parameter alias="entities.location"
	 * @required
	 */
	private String configFile;

	/**
	 * Setter of file.
	 *
	 * @param file
	 *            {@link String} the file to set.
	 */
	public void setFile(final String file) {
		configFile = file;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("generation des entitées\n");

		try {
			// load XML file
			Document document;
			SAXBuilder build = new SAXBuilder();
			document = build.build(new File(configFile));
			Element root = document.getRootElement();

			String author = root.getChildText("author");

			Map<String, List<String>> imports = new Hashtable<>();
			Map<String, List<String>> testImports = new Hashtable<>();
			Map<String, String> tests = new Hashtable<>();
			// load imports
			for (Element cls : new ArrayList<Element>(root.getChild("classes").getChildren())) {
				String className = cls.getAttributeValue("name");
				if (className == null) {
					className = cls.getName();
				}
				if (cls.getChild("imports") != null) {
					for (Element imp : new ArrayList<Element>(cls.getChild("imports").getChildren())) {
						List<String> i = imports.get(className);
						if (i == null) {
							i = new ArrayList<>();
							imports.put(className, i);
						}
						if (!"".equals(imp.getText())) {
							i.add(imp.getText());
						} else {
							i.add(imp.getName());
						}
					}
				}
				if (imports.get(className) != null) {
					testImports.put(className, new ArrayList<>(imports.get(className)));
				}
				if (cls.getChild("testImports") != null) {
					for (Element imp : new ArrayList<Element>(cls.getChild("testImports").getChildren())) {
						List<String> i = testImports.get(className);
						if (i == null) {
							i = new ArrayList<>();
							testImports.put(className, i);
						}
						if (!"".equals(imp.getText())) {
							i.add(imp.getText());
						} else {
							i.add(imp.getName());
						}
					}
				}
				if (cls.getChild("testImportsStatic") != null) {
					for (Element imp : new ArrayList<Element>(cls.getChild("testImportsStatic").getChildren())) {
						List<String> i = testImports.get(className);
						if (i == null) {
							i = new ArrayList<>();
							testImports.put(className, i);
						}
						if (!"".equals(imp.getText())) {
							i.add("static " + imp.getText());
						} else {
							i.add("static " + imp.getName());
						}
					}
				}
				tests.put(className, cls.getChildText("testValue"));
			}
			List<JavaFile> javaFiles = new ArrayList<>();
			// for each package
			for (Element pack : new ArrayList<Element>(root.getChild("packages").getChildren())) {
				String packageName = pack.getAttributeValue("name");
				// create main package
				String mainPackage = destination + "/src/main/java/" + packageName.replace(".", File.separator);
				createDir(mainPackage);

				// for each entities
				if (pack.getChild("entities") != null) {
					// create test package
					String testPackage =
							destination + "/src/test/java/" + packageName.replace(".", File.separator) + File.separator
									+ "test";
					createDir(testPackage);

					for (Element entity : new ArrayList<Element>(pack.getChild("entities").getChildren())) {
						// create entities
						String entityName = entity.getAttributeValue("name");
						if (entityName == null) {
							entityName = entity.getName();
						}
						boolean immutable = !Boolean.valueOf(entity.getAttributeValue("notImmutable"));
						boolean comparable = !Boolean.valueOf(entity.getAttributeValue("notComparable"));
						boolean testable = !Boolean.valueOf(entity.getAttributeValue("notTestable"));
						boolean serializable = !Boolean.valueOf(entity.getAttributeValue("notSerializable"));

						List<Field> fields = new ArrayList<>();
						// create test for entities
						for (Element property : new ArrayList<Element>(entity.getChild("properties").getChildren())) {
							String propertyName = property.getAttributeValue("name");
							if (propertyName == null) {
								propertyName = property.getName();
							}
							boolean optional = Boolean.valueOf(property.getAttributeValue("optional"));
							boolean immutableField = !Boolean.valueOf(property.getAttributeValue("notImmutable"));
							boolean fieldComparable = !Boolean.valueOf(property.getAttributeValue("notComparable"));
							fields.add(Field.builder().isFinal(immutable && immutableField).isOptional(optional)
									.isComparable(fieldComparable).javadoc(null).name(propertyName)
									.type(property.getAttributeValue("type")).build());
						}

						javaFiles.add(JavaFile.builder().author(author).imports(imports).testImports(testImports)
								.tests(tests).isComparable(comparable).isTestable(testable)
								.isSerializable(serializable).name(entityName).packageName(packageName).javadoc(null)
								.fields(fields).build());
					}

					writeGlobalTestCase(testPackage, packageName);
					createDir(destination + "/src/test/config/");
					writeApplicationContextTest(destination + "/src/test/config/", packageName);
				}

				// for each enums
				if (pack.getChild("enums") != null) {
					for (Element entity : new ArrayList<Element>(pack.getChild("enums").getChildren())) {
						String enumName = entity.getName();
						List<String> enums = new ArrayList<>();
						for (Element element : new ArrayList<Element>(entity.getChildren())) {
							enums.add(element.getName());
						}
						JavaEnumeration javaEnumeration =
								JavaEnumeration.builder().author(author).enums(enums).javadoc(null).name(enumName)
										.packageName(packageName).build();

						// create enum
						javaEnumeration.write(destination + "/src/main/java/");
						// add test case
						tests.put(enumName, javaEnumeration.getEnumTest());
						// add import
						imports.put(enumName, Arrays.asList(packageName + "." + enumName));
						// add test imports
						testImports.put(enumName, Arrays.asList("static " + javaEnumeration.getTest()));

						// add tests for list of entities
						String listName = "List<" + javaEnumeration.getName() + ">";
						tests.put(listName, "asList(" + javaEnumeration.getTest() + ")");
						// add import for test List
						List<String> tmp = new ArrayList<>();
						testImports.put(listName, tmp);
						tmp.add("static java.util.Arrays.asList");
						// add import for list of list entities
						tmp = new ArrayList<>();
						imports.put(listName, tmp);
						tmp.add("java.util.List");
						tmp.add(javaEnumeration.getPackageName() + "." + javaEnumeration.getName());

					}
				}

			}

			// generate tests for owned entities
			for (JavaFile boucle : javaFiles) {
				if (!testImports.containsKey(boucle.getName())) {
					for (JavaFile file : javaFiles) {
						if (!testImports.containsKey(file.getName()) && file.GetMissingTest() == null) {
							// add test
							tests.put(file.getName(), file.getTestbuilder(""));
							// add import for test
							List<String> tmp = new ArrayList<>();
							testImports.put(file.getName(), tmp);
							tmp.add(file.getPackageName() + "." + file.getName());
							tmp.addAll(file.getTestImportList());

							// add tests for list of entities
							String listName = "List<" + file.getName() + ">";
							tests.put(listName, "asList(" + file.getTestbuilder("") + ")");
							// add import for test List
							tmp = new ArrayList<>();
							testImports.put(listName, tmp);

							tmp.add(file.getPackageName() + "." + file.getName());
							tmp.add("static java.util.Arrays.asList");
							tmp.addAll(file.getTestImportList());
							// add import for list of list entities
							tmp = new ArrayList<>();
							imports.put(listName, tmp);
							tmp.add("java.util.List");
							tmp.add(file.getPackageName() + "." + file.getName());
						}
					}
				}
			}
			// write java file
			for (JavaFile file : javaFiles) {
				file.write(destination + "/src/main/java/");
				file.writeTest(destination + "/src/test/java/");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new MojoFailureException(e.getMessage());
		}
	}

	/**
	 * create Directories for package.
	 *
	 * @param packageName
	 *            String the package name
	 * @return true if directory is created, false otherwise
	 */
	public static boolean createDir(final String packageName) {
		File file = new File(packageName);
		return file.mkdirs();
	}

	/**
	 * write the ApplicationContextTest.xml file.
	 *
	 * @param folder
	 *            {@link String} folder directory
	 * @param packageName
	 *            packageName
	 * @throws IOException
	 *             if error occurs.
	 */
	public void writeApplicationContextTest(final String folder, final String packageName) throws IOException {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		tmp.append("<beans xmlns=\"http://www.springframework.org/schema/beans\"\n");
		tmp.append("\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
		tmp.append("\txmlns:context=\"http://www.springframework.org/schema/context\"\n");
		tmp.append("\txsi:schemaLocation=\"http://www.springframework.org/schema/beans\n");
		tmp.append("\t\thttp://www.springframework.org/schema/beans/spring-beans.xsd\n");
		tmp.append("\t\thttp://www.springframework.org/schema/context \n");
		tmp.append("\t\thttp://www.springframework.org/schema/context/spring-context-3.1.xsd\">\n");
		tmp.append("\n");
		tmp.append("\t<context:annotation-config />\n");
		tmp.append("\n");
		tmp.append("\t<context:component-scan base-package=\"" + packageName + "\" />\n");
		tmp.append("\n");
		tmp.append("</beans>\n");

		try (FileWriter fw = new FileWriter(new File(folder + File.separator + "applicationContextTest.xml"))) {
			fw.write(tmp.toString());
			fw.flush();
		}
	}

	/**
	 * write GlobalTestCase.java.
	 *
	 * @param folder
	 *            {@link String} folder name
	 * @param packageName
	 *            {@link String} package name
	 * @throws IOException
	 *             if error occurs.
	 */
	public void writeGlobalTestCase(final String folder, final String packageName) throws IOException {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("package " + packageName + ".test;\n");
		tmp.append("\n");
		tmp.append("import org.junit.AfterClass;\n");
		tmp.append("import org.junit.Ignore;\n");
		tmp.append("import org.junit.runner.RunWith;\n");
		tmp.append("import org.slf4j.Logger;\n");
		tmp.append("import org.slf4j.LoggerFactory;\n");
		tmp.append("import org.springframework.test.context.ContextConfiguration;\n");
		tmp.append("import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;\n");
		tmp.append("\n");
		tmp.append("/**\n");
		tmp.append(" * Abstract class for Test classes.\n");
		tmp.append(" * <p>\n");
		tmp.append(" * All test classes must inherit this class.\n");
		tmp.append(" */\n");
		tmp.append("@RunWith(SpringJUnit4ClassRunner.class)\n");
		tmp.append("@ContextConfiguration(\"/applicationContextTest.xml\")\n");
		tmp.append("@Ignore\n");
		tmp.append("public class GlobalTestCase {\n");
		tmp.append("\n");
		tmp.append("\t/** default Constructor. */\n");
		tmp.append("\tprotected GlobalTestCase() {\n");
		tmp.append("\t\tsuper();\n");
		tmp.append("\t}\n");
		tmp.append("\t\n");
		tmp.append("\t/** logger. */\n");
		tmp.append("\tprivate static final Logger LOGGER = LoggerFactory.getLogger(GlobalTestCase.class);\n");
		tmp.append("\t\n");
		tmp.append("\t/** Memory dump. */\n");
		tmp.append("\t@AfterClass\n");
		tmp.append("\tpublic static void afterClass() {\n");
		tmp.append("\t\tLOGGER.info(\"Free memory  : \" + Runtime.getRuntime().freeMemory() + \" bytes\");\n");
		tmp.append("\t\tLOGGER.info(\"Max memory   : \" + Runtime.getRuntime().maxMemory() + \" bytes\");\n");
		tmp.append("\t\tLOGGER.info(\"Total memory : \" + Runtime.getRuntime().totalMemory() + \" bytes\");\n");
		tmp.append("\t}\n");
		tmp.append("}\n");

		try (FileWriter fw = new FileWriter(new File(folder + File.separator + "GlobalTestCase.java"))) {
			fw.write(tmp.toString());
			fw.flush();
		}
	}
}
