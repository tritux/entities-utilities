package com.tritux.common.entities;

import com.tritux.common.utils.Equals;
import com.tritux.common.utils.HashCode;
import com.tritux.common.utils.JsonUtils;

/**
 * Represents an entity field/property for the generated entity classes
 *
 * @author Tritux
 *
 */
public class Field {

	private static final int N_1000 = 1000;

	/** {@link Boolean} isFinal. */
	private final Boolean isFinal;

	/** {@link Boolean} isOptional. */
	private final Boolean isOptional;

	/** {@link Boolean} isComparable. */
	private final Boolean isComparable;

	/** {@link String} javadoc. */
	private final String javadoc;

	/** {@link String} name. */
	private final String name;

	/** {@link String} type. */
	private final String type;

	/**
	 * default deprecated Constructor.
	 *
	 * @deprecated
	 */
	@Deprecated
	protected Field() {
		isFinal = null;
		isOptional = null;
		isComparable = null;
		javadoc = null;
		name = null;
		type = null;
	}

	/**
	 * Constructor.
	 *
	 * @param builder
	 *            the {@link Builder} to use.
	 */
	protected Field(final Builder builder) {
		isFinal = builder.getIsFinal();
		isOptional = builder.getIsOptional();
		isComparable = builder.getIsComparable();
		javadoc = builder.getJavadoc();
		name = builder.getName();
		type = builder.getType();
	}

	/**
	 * builder of {@link Field}.
	 *
	 * @return new {@link Builder} for {@link Field}
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder for {@link Field}.
	 *
	 * @author Tritux
	 *
	 */
	public static class Builder {

		/** {@link Boolean} isFinal. */
		private Boolean isFinal;

		/** {@link Boolean} isOptional. */
		private Boolean isOptional;

		/** {@link Boolean} isComparable. */
		private Boolean isComparable;

		/** {@link String} javadoc. */
		private String javadoc;

		/** {@link String} name. */
		private String name;

		/** {@link String} type. */
		private String type;

		/** default protected Constructor. */
		protected Builder() {
			super();
		}

		/**
		 * Getter of isFinal.
		 *
		 * @return {@link Boolean} the isFinal
		 */
		public Boolean getIsFinal() {
			return isFinal;
		}

		/**
		 * Getter of isOptional.
		 *
		 * @return {@link Boolean} the isOptional
		 */
		public Boolean getIsOptional() {
			return isOptional;
		}

		/**
		 * Getter of javadoc.
		 *
		 * @return {@link String} the javadoc
		 */
		public String getJavadoc() {
			return javadoc;
		}

		/**
		 * Getter of name.
		 *
		 * @return {@link String} the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Getter of type.
		 *
		 * @return {@link String} the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * Getter of isComparable.
		 *
		 * @return {@link Boolean} the isComparable
		 */
		public Boolean getIsComparable() {
			return isComparable;
		}

		/**
		 * Setter of isComparable.
		 *
		 * @param isComparable
		 *            {@link Boolean} the isComparable to set.
		 * @return this {@link Builder}
		 */
		public Builder isComparable(final Boolean isComparable) {
			this.isComparable = isComparable;
			return this;
		}

		/**
		 * Setter of isFinal.
		 *
		 * @param isFinal
		 *            {@link Boolean} the isFinal to set.
		 * @return this {@link Builder}
		 */
		public Builder isFinal(final Boolean isFinal) {
			this.isFinal = isFinal;
			return this;
		}

		/**
		 * Setter of isOptional.
		 *
		 * @param isOptional
		 *            {@link Boolean} the isOptional to set.
		 * @return this {@link Builder}
		 */
		public Builder isOptional(final Boolean isOptional) {
			this.isOptional = isOptional;
			return this;
		}

		/**
		 * Setter of javadoc.
		 *
		 * @param javadoc
		 *            {@link String} the javadoc to set.
		 * @return this {@link Builder}
		 */
		public Builder javadoc(final String javadoc) {
			this.javadoc = javadoc;
			return this;
		}

		/**
		 * Setter of name.
		 *
		 * @param name
		 *            {@link String} the name to set.
		 * @return this {@link Builder}
		 */
		public Builder name(final String name) {
			this.name = name;
			return this;
		}

		/**
		 * Setter of type.
		 *
		 * @param type
		 *            {@link String} the type to set.
		 * @return this {@link Builder}
		 */
		public Builder type(final String type) {
			this.type = type;
			return this;
		}

		/**
		 * build new {@link Field}.
		 *
		 * @return new {@link Field} from this {@link Builder}
		 */
		public Field build() {
			return new Field(this);
		}

	}

	/**
	 * Getter of isFinal.
	 *
	 * @return {@link Boolean} the isFinal
	 */
	public Boolean getIsFinal() {
		return isFinal;
	}

	/**
	 * Getter of isOptional.
	 *
	 * @return {@link Boolean} the isOptional
	 */
	public Boolean getIsOptional() {
		return isOptional;
	}

	/**
	 * Getter of isComparable.
	 *
	 * @return {@link Boolean} the isComparable
	 */
	public Boolean getIsComparable() {
		return isComparable;
	}

	/**
	 * Getter of javadoc.
	 *
	 * @return {@link String} the javadoc
	 */
	public String getJavadoc() {
		return javadoc;
	}

	/**
	 * Getter of name.
	 *
	 * @return {@link String} the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter of type.
	 *
	 * @return {@link String} the type
	 */
	public String getType() {
		return type;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Field)) {
			return false;
		}
		Field field = (Field) obj;
		return Equals.TRUE.andEquals(getIsFinal(), field.getIsFinal())
				.andEquals(getIsOptional(), field.getIsOptional()).andEquals(getJavadoc(), field.getJavadoc())
				.andEquals(getName(), field.getName()).andEquals(getType(), field.getType()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCode(null).append(getIsFinal()).append(getIsOptional())
				.append(getJavadoc()).append(getName()).append(getType()).hashCode();
	}

	@Override
	public String toString() {
		try {
			return JsonUtils.toPrettyJson(this);
		} catch (com.fasterxml.jackson.core.JsonProcessingException jpe) {
			return super.toString();
		}
	}

	/**
	 * getDeclaration.
	 *
	 * @param tab
	 *            tab before text
	 * @return {@link String} representing declaration for {@link Field}
	 */
	public String getDeclaration(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		if (getJavadoc() == null) {
			tmp.append(tab + "/** {@link " + getType() + "} " + getName() + ". */\n");
		} else {
			tmp.append(tab + "/**\n");
			tmp.append(tab + "\t * {@link " + getType() + "} " + getName() + ". */\n");
			tmp.append(tab + " *\t" + getJavadoc() + "\n");
			tmp.append(tab + " */\n");
		}
		if (getType().equals("LocalDate")) {
			tmp.append(tab
					+ "@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = com.tritux.common.date.LocalDateDeserializer.class)\n");
			tmp.append(tab
					+ "@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = com.tritux.common.date.LocalDateSerializer.class)\n");
		}
		if (getType().equals("LocalDateTime")) {
			tmp.append(tab
					+ "@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = com.tritux.common.date.LocalDateTimeDeserializer.class)\n");
			tmp.append(tab
					+ "@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = com.tritux.common.date.LocalDateTimeSerializer.class)\n");
		}
		tmp.append(tab + "protected " + (getIsFinal() ? "final " : "") + getType() + " " + getName() + ";\n");

		return tmp.toString();
	}

	/**
	 * getGetter.
	 *
	 * @param tab
	 *            tab before text
	 * @return {@link String} representing getter for {@link Field}
	 */
	public String getGetter(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/**\n");
		tmp.append(tab + " * Getter of " + getName() + ".\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " * @return {@link " + getType() + "} the " + getName() + "\n");
		tmp.append(tab + " */\n");
		tmp.append(tab + "public " + getType() + " get" + getUpperName() + "() {\n");
		tmp.append(tab + "\treturn " + getName() + ";\n");
		tmp.append(tab + "}\n");

		return tmp.toString();
	}

	/**
	 * getSetter.
	 *
	 * @param tab
	 *            tab before text
	 * @return {@link String} representing setter for {@link Field}
	 */
	public String getSetter(final String tab) {
		if (getIsFinal()) {
			return "";
		}
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/**\n");
		tmp.append(tab + " * Setter of " + getName() + ".\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " * @param " + getName() + "\n");
		tmp.append(tab + " *\t\t\t{@link " + getType() + "} the " + getName() + " to set.\n");
		tmp.append(tab + " */\n");
		tmp.append(tab + "public void set" + getUpperName() + "(final " + getType() + " " + getName() + ") {\n");
		tmp.append(tab + "\tthis." + getName() + " = " + getName() + ";\n");
		tmp.append(tab + "}\n");

		return tmp.toString();
	}

	/**
	 * getAppender.
	 *
	 * @param tab
	 *            tab before text
	 * @return {@link String} representing appender for {@link Field}
	 */
	public String getAppender(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/**\n");
		tmp.append(tab + " * Setter of " + getName() + ".\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " * @param " + getName() + "\n");
		tmp.append(tab + " *\t\t\t{@link " + getType() + "} the " + getName() + " to set.\n");
		if (getJavadoc() != null) {
			tmp.append(tab + " *\t" + getJavadoc() + "\n");
		}
		tmp.append(tab + " * @return this {@link Builder}\n");
		tmp.append(tab + " */\n");
		tmp.append(tab + "public Builder " + getName() + "(final " + getType() + " " + getName() + ") {\n");
		tmp.append(tab + "\tthis." + getName() + " = " + getName() + ";\n");
		tmp.append(tab + "\treturn this;\n");
		tmp.append(tab + "}\n");

		return tmp.toString();
	}

	/**
	 * getUpperName.
	 *
	 * @return String upperName
	 */
	public String getUpperName() {
		return getName().substring(0, 1).toUpperCase() + getName().substring(1);
	}

}
