package com.tritux.common.entities;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.tritux.common.utils.Equals;
import com.tritux.common.utils.HashCode;
import com.tritux.common.utils.JsonUtils;

/**
 * Represents the corresponding java file for a generated entity class
 *
 * @author Tritux
 *
 */
public class JavaFile {

	private static final int N_1000 = 1000;

	/** {@link String} author. */
	private final String author;

	/** {@link List<FieldMethods>} fields. */
	private final List<Field> fields;

	/** {@link Map<String,List<String>>} imports. */
	private final Map<String, List<String>> imports;

	/** {@link Map<String,List<String>>} testImports. */
	private final Map<String, List<String>> testImports;

	/** {@link String} javadoc. */
	private final String javadoc;

	/** {@link String} name. */
	private final String name;

	/** {@link String} packageName. */
	private final String packageName;

	/** {@link String} parentName. */
	private final String parentName;

	/** {@link Map<String,String>} tests. */
	private final Map<String, String> tests;

	/** {@link Boolean} comparable. */
	private final Boolean isComparable;

	/** {@link Boolean} isTestable. */
	private final Boolean isTestable;

	/** {@link Boolean} isSerializable. */
	private final Boolean isSerializable;

	/**
	 * default deprecated Constructor.
	 *
	 * @deprecated
	 */
	@Deprecated
	protected JavaFile() {
		author = null;
		fields = null;
		imports = null;
		testImports = null;
		javadoc = null;
		name = null;
		packageName = null;
		parentName = null;
		tests = null;
		isComparable = true;
		isTestable = true;
		isSerializable = null;
	}

	/**
	 * Constructor.
	 *
	 * @param builder
	 *            the {@link Builder} to use.
	 */
	protected JavaFile(final Builder builder) {
		author = builder.author;
		fields = builder.fields;
		imports = builder.imports;
		testImports = builder.testImports;
		javadoc = builder.javadoc;
		name = builder.name;
		packageName = builder.packageName;
		parentName = builder.parentName;
		tests = builder.tests;
		isComparable = builder.isComparable;
		isTestable = builder.isTestable;
		isSerializable = builder.isSerializable;
	}

	/**
	 * builder of {@link JavaFile}.
	 *
	 * @return new {@link Builder} for {@link JavaFile}
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder for {@link JavaFile}.
	 *
	 * @author Tritux
	 *
	 */
	public static class Builder {

		/** {@link String} author. */
		protected String author;

		/** {@link List<FieldMethods>} fields. */
		protected List<Field> fields;

		/** {@link Map<String,List<String>>} imports. */
		protected Map<String, List<String>> imports;

		/** {@link Map<String,List<String>>} testImports. */
		protected Map<String, List<String>> testImports;

		/** {@link String} javadoc. */
		protected String javadoc;

		/** {@link String} name. */
		protected String name;

		/** {@link String} packageName. */
		protected String packageName;

		/** {@link String} parentName. */
		protected String parentName;

		/** {@link Map<String,String>} tests. */
		protected Map<String, String> tests;

		/** {@link Boolean} isComparable. */
		protected Boolean isComparable;

		/** {@link Boolean} isTestable. */
		protected Boolean isTestable;

		/** {@link Boolean} isSerializable. */
		protected Boolean isSerializable;

		/** default protected Constructor. */
		protected Builder() {
			super();
		}

		/**
		 * Setter of isSerializable.
		 *
		 * @param isSerializable
		 *            {@link Boolean} the isSerializable to set.
		 * @return this {@link Builder}
		 */
		public Builder isSerializable(final Boolean isSerializable) {
			this.isSerializable = isSerializable;
			return this;
		}

		/**
		 * Setter of isTestable.
		 *
		 * @param isTestable
		 *            {@link Boolean} the isTestable to set.
		 * @return this {@link Builder}
		 */
		public Builder isTestable(final Boolean isTestable) {
			this.isTestable = isTestable;
			return this;
		}

		/**
		 * Setter of isComparable.
		 *
		 * @param isComparable
		 *            {@link Boolean} the isComparable to set.
		 * @return this {@link Builder}
		 */
		public Builder isComparable(final Boolean isComparable) {
			this.isComparable = isComparable;
			return this;
		}

		/**
		 * Setter of author.
		 *
		 * @param author
		 *            {@link String} the author to set.
		 * @return this {@link Builder}
		 */
		public Builder author(final String author) {
			this.author = author;
			return this;
		}

		/**
		 * Setter of fields.
		 *
		 * @param fields
		 *            {@link List<FieldMethods>} the fields to set.
		 * @return this {@link Builder}
		 */
		public Builder fields(final List<Field> fields) {
			this.fields = fields;
			return this;
		}

		/**
		 * Setter of imports.
		 *
		 * @param imports
		 *            {@link Map<String,List<String>>} the imports to set.
		 * @return this {@link Builder}
		 */
		public Builder imports(final Map<String, List<String>> imports) {
			this.imports = imports;
			return this;
		}

		/**
		 * Setter of testImports.
		 *
		 * @param testImports
		 *            {@link Map<String,List<String>>} the testImports to set.
		 * @return this {@link Builder}
		 */
		public Builder testImports(final Map<String, List<String>> testImports) {
			this.testImports = testImports;
			return this;
		}

		/**
		 * Setter of javadoc.
		 *
		 * @param javadoc
		 *            {@link String} the javadoc to set.
		 * @return this {@link Builder}
		 */
		public Builder javadoc(final String javadoc) {
			this.javadoc = javadoc;
			return this;
		}

		/**
		 * Setter of name.
		 *
		 * @param name
		 *            {@link String} the name to set.
		 * @return this {@link Builder}
		 */
		public Builder name(final String name) {
			this.name = name;
			return this;
		}

		/**
		 * Setter of packageName.
		 *
		 * @param packageName
		 *            {@link String} the packageName to set.
		 * @return this {@link Builder}
		 */
		public Builder packageName(final String packageName) {
			this.packageName = packageName;
			return this;
		}

		/**
		 * Setter of parentName.
		 *
		 * @param parentName
		 *            {@link String} the parentName to set.
		 * @return this {@link Builder}
		 */
		public Builder parentName(final String parentName) {
			this.parentName = parentName;
			return this;
		}

		/**
		 * Setter of tests.
		 *
		 * @param tests
		 *            {@link Map<String,String>} the tests to set.
		 * @return this {@link Builder}
		 */
		public Builder tests(final Map<String, String> tests) {
			this.tests = tests;
			return this;
		}

		/**
		 * build new {@link JavaFile}.
		 *
		 * @return new {@link JavaFile} from this {@link Builder}
		 */
		public JavaFile build() {
			return new JavaFile(this);
		}

	}

	/**
	 * Getter of author.
	 *
	 * @return {@link String} the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Getter of fields.
	 *
	 * @return {@link List<FieldMethods>} the fields
	 */
	public List<Field> getFields() {
		return fields;
	}

	/**
	 * Getter of imports.
	 *
	 * @return {@link Map<String,List<String>>} the imports
	 */
	public Map<String, List<String>> getImports() {
		return imports;
	}

	/**
	 * Getter of testImports.
	 *
	 * @return {@link Map<String,List<String>>} the testImports
	 */
	public Map<String, List<String>> getTestImports() {
		return testImports;
	}

	/**
	 * Getter of javadoc.
	 *
	 * @return {@link String} the javadoc
	 */
	public String getJavadoc() {
		return javadoc;
	}

	/**
	 * Getter of name.
	 *
	 * @return {@link String} the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter of packageName.
	 *
	 * @return {@link String} the packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Getter of parentName.
	 *
	 * @return {@link String} the parentName
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * Getter of tests.
	 *
	 * @return {@link Map<String,String>} the tests
	 */
	public Map<String, String> getTests() {
		return tests;
	}

	/**
	 * Getter of isComparable.
	 *
	 * @return {@link Boolean} the isComparable
	 */
	public Boolean getIsComparable() {
		return isComparable;
	}

	/**
	 * Getter of isTestable.
	 *
	 * @return {@link Boolean} the isTestable
	 */
	public Boolean getIsTestable() {
		return isTestable;
	}

	/**
	 * Getter of isSerializable.
	 *
	 * @return {@link Boolean} the isSerializable
	 */
	public Boolean getIsSerializable() {
		return isSerializable;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof JavaFile)) {
			return false;
		}
		JavaFile javafile = (JavaFile) obj;
		return Equals.TRUE.andEquals(getAuthor(), javafile.getAuthor())
				.andEquals(getFields(), javafile.getFields()).andEquals(getImports(), javafile.getImports())
				.andEquals(getJavadoc(), javafile.getJavadoc()).andEquals(getName(), javafile.getName())
				.andEquals(getPackageName(), javafile.getPackageName())
				.andEquals(getParentName(), javafile.getParentName()).andEquals(getTests(), javafile.getTests())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCode(null).append(getAuthor()).append(getFields())
				.append(getImports()).append(getJavadoc()).append(getName()).append(getPackageName())
				.append(getParentName()).append(getTests()).hashCode();
	}

	@Override
	public String toString() {
		try {
			return JsonUtils.toPrettyJson(this);
		} catch (com.fasterxml.jackson.core.JsonProcessingException jpe) {
			return super.toString();
		}
	}

	/**
	 * getDefaultConstructor.
	 *
	 * @param tab
	 *            tab before text
	 * @return default constructor
	 */
	public String getDefaultConstructor(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/** default protected Constructor. */\n");
		tmp.append(tab + "protected " + getName() + "() {\n");
		tmp.append(tab + "\tsuper();\n");
		for (Field field : getFields()) {
			if (field.getIsFinal()) {
				tmp.append(tab + "\t" + field.getName() + " = null;\n");
			}
		}
		tmp.append(tab + "}\n");

		return tmp.toString();
	}

	/**
	 * getDeprecatedConstructor.
	 *
	 * @param tab
	 *            tab before text
	 * @return deprecated constructor
	 */
	public String getDeprecatedConstructor(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/**\n");
		tmp.append(tab + " * default deprecated Constructor.\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " * @deprecated\n");
		tmp.append(tab + " */\n");
		tmp.append(tab + "@Deprecated\n");
		tmp.append(tab + "protected " + getName() + "() {\n");
		for (Field field : getFields()) {
			tmp.append(tab + "\t" + field.getName() + " = null;\n");
		}
		tmp.append(tab + "}\n");

		return tmp.toString();
	}

	/**
	 * getBuilderConstructor.
	 *
	 * @return builder constructor
	 */
	public String getBuilderConstructor() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("\t/**\n");
		tmp.append("\t * Constructor.\n");
		tmp.append("\t *\n");
		tmp.append("\t * @param builder the {@link Builder} to use.\n");
		tmp.append("\t */\n");
		tmp.append("\tprotected " + getName() + "(final Builder builder) {\n");
		for (Field field : getFields()) {
			tmp.append("\t\t" + field.getName() + " = builder." + field.getName() + ";\n");
		}
		tmp.append("\t\n");
		tmp.append("\t\t// check parameters\n");
		for (Field field : getFields()) {
			if (!field.getIsOptional()) {
				tmp.append("\t\tcom.tritux.common.utils.Null.checkNotNullOrEmpty(\"" + field.getName()
						+ "\", " + field.getName() + ");\n");
			}
		}
		tmp.append("\t}\n");

		return tmp.toString();
	}

	/**
	 * getBuilder.
	 *
	 * @param tab
	 *            tab before text
	 * @return builder
	 */
	public String getBuilder(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/**\n");
		tmp.append(tab + " * builder of {@link " + getName() + "}.\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " * @return new {@link Builder} for {@link " + getName() + "}\n");
		tmp.append(tab + " */\n");
		tmp.append(tab + "public static Builder builder() {\n");
		tmp.append(tab + "\treturn new Builder();\n");
		tmp.append(tab + "}\n");

		List<Field> fields = new ArrayList<>();
		for (Field field : getFields()) {
			fields.add(Field.builder().isFinal(false).javadoc(field.getJavadoc()).name(field.getName())
					.type(field.getType()).build());
		}
		JavaFile builder =
				JavaFile.builder().author(getAuthor()).name("Builder").parentName(getName())
						.packageName(getPackageName()).fields(fields).build();

		tmp.append(builder.getStaticClass(tab));

		return tmp.toString();
	}

	/**
	 * getStaticClass.
	 *
	 * @param tab
	 *            tab before text
	 * @return static class
	 */
	public String getStaticClass(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/**\n");
		tmp.append(tab + " * " + getName() + " for {@link " + getParentName() + "}.\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " * @author " + getAuthor() + "\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " */\n");
		tmp.append(tab + "public static class " + getName() + " {\n");
		tmp.append(tab + "\t\n");
		for (Field field : getFields()) {
			tmp.append(field.getDeclaration(tab + "\t")).append("\n");
		}
		tmp.append(getDefaultConstructor(tab + "\t")).append("\n");
		for (Field field : getFields()) {
			tmp.append(field.getAppender(tab + "\t")).append("\n");
		}
		tmp.append(getBuild(tab + "\t")).append("\n");

		tmp.append(tab + "}\n");

		return tmp.toString();
	}

	/**
	 * getBuild.
	 *
	 * @param tab
	 *            tab before text
	 * @return build method
	 */
	public String getBuild(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append(tab + "/**\n");
		tmp.append(tab + " * build new {@link " + getParentName() + "}.\n");
		tmp.append(tab + " *\n");
		tmp.append(tab + " * @return new {@link " + getParentName() + "} from this {@link Builder}\n");
		tmp.append(tab + " */\n");
		tmp.append(tab + "public " + getParentName() + " build() {\n");
		tmp.append(tab + "\treturn new " + getParentName() + "(this);\n");
		tmp.append(tab + "}\n");

		return tmp.toString();
	}

	/**
	 * getJavaFile.
	 *
	 * @return the java file
	 */
	public String getJavaFile() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("package " + getPackageName() + ";\n");
		tmp.append("\n");
		tmp.append("import org.slf4j.LoggerFactory;\n");
		tmp.append(getImport());
		tmp.append("\n");
		tmp.append("/**\n");
		tmp.append(" * " + getName() + ".\n");
		if (getJavadoc() != null) {
			tmp.append(" * " + getJavadoc() + ".\n");
		}
		tmp.append(" *\n");
		tmp.append(" * this class is auto generated by the com.tritux.common project : entities-maven-plugin,\n");
		tmp.append(" * do not edit this class directly, all change will be lost on next build ...\n");
		tmp.append(" *\n");
		tmp.append(" * @author " + getAuthor() + "\n");
		tmp.append(" *\n");
		tmp.append(" */\n");
		tmp.append("public class " + getName() + (getIsComparable() ? " implements Comparable<" + getName() + ">" : "")
				+ " {\n");
		tmp.append("\n");
		for (Field field : getFields()) {
			tmp.append(field.getDeclaration("\t")).append("\n");
		}
		tmp.append(getDeprecatedConstructor("\t")).append("\n");
		tmp.append(getBuilderConstructor()).append("\n");
		tmp.append(getBuilder("\t"));
		for (Field field : getFields()) {
			tmp.append(field.getGetter("\t")).append("\n");
			if (!field.getIsFinal()) {
				tmp.append(field.getSetter("\t")).append("\n");
			}
		}
		tmp.append(getEquals()).append("\n");
		tmp.append(getHashCode()).append("\n");
		tmp.append(getToString()).append("\n");
		if (getIsComparable()) {
			tmp.append(getcompareTo()).append("\n");
		}
		tmp.append("\n");
		tmp.append("}\n");

		return tmp.toString();
	}

	/**
	 * write class to folder.
	 *
	 * @param folder
	 *            String destination folder
	 * @throws IOException
	 *             if error occurs
	 */
	public void write(final String folder) throws IOException {

		try (FileWriter fw =
				new FileWriter(new File(folder + File.separator + getPackageName().replace(".", File.separator)
						+ File.separator + getName() + ".java"))) {
			fw.write(getJavaFile());
			fw.flush();
		}
	}

	/**
	 * write Test class.
	 *
	 * @param folder
	 *            String destination folder
	 * @throws IOException
	 *             if error occurs
	 */
	public void writeTest(final String folder) throws IOException {
		if (getIsTestable()) {
			try (FileWriter fw =
					new FileWriter(new File(folder + File.separator + getPackageName().replace(".", File.separator)
							+ File.separator + "test" + File.separator + getName() + "Test.java"))) {
				fw.write(getJavaTestFile());
				fw.flush();
			}
		}
	}

	/**
	 * getEquals.
	 *
	 * @return equals method
	 */
	public String getEquals() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("\t@Override\n");
		tmp.append("\tpublic boolean equals(final Object obj) {\n");
		tmp.append("\t\tif (!(obj instanceof " + getName() + ")) {\n");
		tmp.append("\t\t\treturn false;\n");
		tmp.append("\t\t}\n");
		tmp.append("\t\t" + getName() + " " + getName().toLowerCase() + " = (" + getName() + ") obj;\n");
		tmp.append("\t\treturn com.tritux.common.utils.Equals.TRUE");
		for (Field field : getFields()) {
			tmp.append("\n\t\t\t.andEquals(get" + field.getUpperName() + "(), " + getName().toLowerCase() + ".get"
					+ field.getUpperName() + "())");
		}
		tmp.append(".isEquals();\n");
		tmp.append("\t}\n");

		return tmp.toString();
	}

	/**
	 * getHashCode.
	 *
	 * @return hash code method
	 */
	public String getHashCode() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("\t@Override\n");
		tmp.append("\tpublic int hashCode() {\n");
		tmp.append("\treturn new com.tritux.common.utils.HashCode(null)");
		for (Field field : getFields()) {
			tmp.append("\n\t\t.append(get" + field.getUpperName() + "())");
		}
		tmp.append(".hashCode();\n");
		tmp.append("\t}\n");

		return tmp.toString();
	}

	/**
	 * getToString.
	 *
	 * @return toString method
	 */
	public String getToString() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("\t@Override\n");
		tmp.append("\tpublic String toString() {\n");
		tmp.append("\t\ttry {\n");
		tmp.append("\t\t\treturn com.tritux.common.utils.JSon.toPrettyJson(this);\n");
		tmp.append("\t\t} catch (com.fasterxml.jackson.core.JsonProcessingException jpe) {\n");
		tmp.append("\t\t\tLoggerFactory.getLogger(" + getName() + ".class).error(\"error with toString\", jpe);\n");
		tmp.append("\t\t\treturn super.toString();\n");
		tmp.append("\t\t}\n");
		tmp.append("\t}\n");

		return tmp.toString();
	}

	/**
	 * getcompareTo.
	 *
	 * @return compareTo method
	 */
	public String getcompareTo() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("\t@Override\n");
		tmp.append("\tpublic int compareTo(final " + getName() + " o) {\n");

		tmp.append("\t\treturn new com.tritux.common.utils.Compare(true, true)");
		for (Field field : getFields()) {
			if (field.getIsComparable()) {
				tmp.append("\n\t\t.andCompare(get" + field.getUpperName() + "(), o.get" + field.getUpperName() + "())");
			}
		}
		tmp.append(".compare();\n");
		tmp.append("\t}\n");

		return tmp.toString();
	}

	/**
	 * getImport.
	 *
	 * @return imports
	 */
	public String getImport() {
		StringBuilder tmp = new StringBuilder(N_1000);

		Set<String> imp = new TreeSet<>();
		for (Field field : getFields()) {
			if (getImports().containsKey(field.getType())) {
				for (String i : getImports().get(field.getType())) {
					imp.add(i.trim());
				}
			}
		}
		for (String i : imp) {
			tmp.append("import " + i + ";\n");
		}
		return tmp.toString();
	}

	/**
	 * getTestImport.
	 *
	 * @return imports for test
	 */
	public String getTestImport() {
		StringBuilder tmp = new StringBuilder(N_1000);

		Set<String> imp = new TreeSet<>();
		for (Field field : getFields()) {
			if (getTestImports().containsKey(field.getType())) {
				for (String i : getTestImports().get(field.getType())) {
					imp.add(i.trim());
				}
			}
		}
		for (String i : imp) {
			tmp.append("import " + i + ";\n");
		}
		return tmp.toString();
	}

	/**
	 * get list of Test Imports.
	 *
	 * @return get list of test imports.
	 */
	public List<String> getTestImportList() {
		Set<String> imp = new TreeSet<>();
		for (Field field : getFields()) {
			if (getTestImports().containsKey(field.getType())) {
				for (String i : getTestImports().get(field.getType())) {
					imp.add(i.trim());
				}
			}
		}
		List<String> imports = new ArrayList<>();
		for (String i : imp) {
			imports.add(i);
		}
		return imports;
	}

	/**
	 * Get Missing Test.
	 *
	 * @return get the entity field missing for test or null, if no missing test
	 */
	public Field GetMissingTest() {
		for (Field f : getFields()) {
			if (getTests().get(f.getType()) == null) {
				return f;
			}
		}
		return null;
	}

	/**
	 * get Test builder.
	 *
	 * @param tab
	 *            the tab car
	 * @return the testBuilder
	 */
	public String getTestbuilder(final String tab) {
		StringBuilder tmp = new StringBuilder(N_1000);
		tmp.append(getName() + ".builder()");
		for (Field f : getFields()) {
			tmp.append(tab + "." + f.getName() + "(" + getTests().get(f.getType()) + ")");
		}
		tmp.append(".build()");
		return tmp.toString();
	}

	/**
	 * getJavaTestFile.
	 *
	 * @return java file for test
	 */
	public String getJavaTestFile() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("package " + getPackageName() + ".test;\n");
		tmp.append("\n");
		if (getIsSerializable()) {
			tmp.append("import static com.tritux.common.utils.JSon.canBeJSonSerialized;\n");
		}
		tmp.append("import static com.tritux.common.utils.Utils.hasProtectedEmptyConstructorEqualsAndHashCodeAndCompareTo;\n");
		tmp.append("import static org.junit.Assert.assertTrue;\n");
		tmp.append("\n");
		tmp.append("import org.junit.Test;\n");
		tmp.append("\n");
		tmp.append(getTestImport());
		tmp.append("import " + getPackageName() + "." + getName() + ";\n");
		tmp.append("\n");
		tmp.append("/**\n");
		tmp.append(" * generic test for {@link " + getName() + "} entity.\n");
		tmp.append(" *\n");
		tmp.append(" * this class is auto generated by the com.tritux.common project : entities-maven-plugin,\n");
		tmp.append(" * do not edit this class directly, all change will be lost on next build ...\n");
		tmp.append(" */\n");
		tmp.append("@SuppressWarnings(\"unused\")\n");
		tmp.append("public class " + getName() + "Test extends GlobalTestCase {\n");
		tmp.append("\n");
		tmp.append("\t/**\n");
		tmp.append("\t * test  for " + getName() + "  methods.\n");
		tmp.append("\t * <li> constructors,\n");
		tmp.append("\t * <li> {@link " + getName() + "#equals(Object)},\n");
		tmp.append("\t * <li> {@link " + getName() + "#hashCode()},\n");
		tmp.append("\t * <li> {@link " + getName() + "#toString()}\n");
		tmp.append("\t * <li> {@link " + getName() + "#compareTo(" + getName() + ")}\n");
		tmp.append("\t */\n");
		tmp.append("\t@Test\n");
		tmp.append("\tpublic void successTest() {\n");
		tmp.append("\t\t" + getName() + " tmp = " + getTestbuilder("\n\t\t\t") + ";\n");
		tmp.append("\t\tassertTrue(hasProtectedEmptyConstructorEqualsAndHashCodeAndCompareTo(" + getName()
				+ ".class, tmp));\n");
		if (getIsSerializable()) {
			tmp.append("\t\tassertTrue(canBeJSonSerialized(tmp));\n");
		}
		tmp.append("\t}\n");

		tmp.append("}\n");

		return tmp.toString();
	}
}
