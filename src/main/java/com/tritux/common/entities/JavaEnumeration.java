package com.tritux.common.entities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.tritux.common.utils.Equals;
import com.tritux.common.utils.HashCode;
import com.tritux.common.utils.JsonUtils;

/**
 * Modelize an enumeration field in an entity class
 *
 * @author Tritux
 *
 */
public class JavaEnumeration {

	private static final int N_1000 = 1000;

	/** {@link String} author. */
	private final String author;

	/** {@link List<FieldMethods>} fields. */
	private final List<String> enums;

	/** {@link String} javadoc. */
	private final String javadoc;

	/** {@link String} name. */
	private final String name;

	/** {@link String} packageName. */
	private final String packageName;

	/**
	 * default deprecated Constructor.
	 *
	 * @deprecated
	 */
	@Deprecated
	protected JavaEnumeration() {
		author = null;
		enums = null;
		javadoc = null;
		name = null;
		packageName = null;
	}

	/**
	 * Constructor.
	 *
	 * @param builder
	 *            the {@link Builder} to use.
	 */
	protected JavaEnumeration(final Builder builder) {
		author = builder.author;
		enums = builder.enums;
		javadoc = builder.javadoc;
		name = builder.name;
		packageName = builder.packageName;
	}

	/**
	 * builder of {@link JavaEnumeration}.
	 *
	 * @return new {@link Builder} for {@link JavaEnumeration}
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder for {@link JavaEnumeration}.
	 *
	 * @author Tritux
	 *
	 */
	public static class Builder {

		/** {@link String} author. */
		protected String author;

		/** {@link List<String>} enums. */
		protected List<String> enums;

		/** {@link String} javadoc. */
		protected String javadoc;

		/** {@link String} name. */
		protected String name;

		/** {@link String} packageName. */
		protected String packageName;

		/** default protected Constructor. */
		protected Builder() {
			super();
		}

		/**
		 * Setter of author.
		 *
		 * @param author
		 *            {@link String} the author to set.
		 * @return this {@link Builder}
		 */
		public Builder author(final String author) {
			this.author = author;
			return this;
		}

		/**
		 * Setter of enums.
		 *
		 * @param author
		 *            List<{@link String} > the author to set.
		 * @return this {@link Builder}
		 */
		public Builder enums(final List<String> enums) {
			this.enums = enums;
			return this;
		}

		/**
		 * Setter of javadoc.
		 *
		 * @param javadoc
		 *            {@link String} the javadoc to set.
		 * @return this {@link Builder}
		 */
		public Builder javadoc(final String javadoc) {
			this.javadoc = javadoc;
			return this;
		}

		/**
		 * Setter of name.
		 *
		 * @param name
		 *            {@link String} the name to set.
		 * @return this {@link Builder}
		 */
		public Builder name(final String name) {
			this.name = name;
			return this;
		}

		/**
		 * Setter of packageName.
		 *
		 * @param packageName
		 *            {@link String} the packageName to set.
		 * @return this {@link Builder}
		 */
		public Builder packageName(final String packageName) {
			this.packageName = packageName;
			return this;
		}

		/**
		 * build new {@link JavaEnumeration}.
		 *
		 * @return new {@link JavaEnumeration} from this {@link Builder}
		 */
		public JavaEnumeration build() {
			return new JavaEnumeration(this);
		}

	}

	/**
	 * Getter of author.
	 *
	 * @return {@link String} the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Getter of fields.
	 *
	 * @return {@link List<FieldMethods>} the fields
	 */
	public List<String> getEnums() {
		return enums;
	}

	/**
	 * Getter of javadoc.
	 *
	 * @return {@link String} the javadoc
	 */
	public String getJavadoc() {
		return javadoc;
	}

	/**
	 * Getter of name.
	 *
	 * @return {@link String} the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter of packageName.
	 *
	 * @return {@link String} the packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof JavaEnumeration)) {
			return false;
		}
		JavaEnumeration javafile = (JavaEnumeration) obj;
		return Equals.TRUE.andEquals(getAuthor(), javafile.getAuthor())
				.andEquals(getEnums(), javafile.getEnums()).andEquals(getJavadoc(), javafile.getJavadoc())
				.andEquals(getName(), javafile.getName()).andEquals(getPackageName(), javafile.getPackageName())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCode(null).append(getAuthor()).append(getEnums())
				.append(getJavadoc()).append(getName()).append(getPackageName()).hashCode();
	}

	@Override
	public String toString() {
		try {
			return JsonUtils.toPrettyJson(this);
		} catch (com.fasterxml.jackson.core.JsonProcessingException jpe) {
			return super.toString();
		}
	}

	/**
	 * getJavaFile.
	 *
	 * @return the java file
	 */
	public String getJavaFile() {
		StringBuilder tmp = new StringBuilder(N_1000);

		tmp.append("package " + getPackageName() + ";\n");
		tmp.append("\n");
		tmp.append("/**\n");
		tmp.append(" * " + getName() + ".\n");
		if (getJavadoc() != null) {
			tmp.append(" * " + getJavadoc() + ".\n");
		}
		tmp.append(" *\n");
		tmp.append(" * this enum is auto generated by the com.tritux.common project : entities-maven-plugin,\n");
		tmp.append(" * do not edit this class directly, all change will be lost on next build ...\n");
		tmp.append(" *\n");
		tmp.append(" * @author " + getAuthor() + "\n");
		tmp.append(" *\n");
		tmp.append(" */\n");
		tmp.append("public enum " + getName() + " {\n");
		tmp.append("\n");
		int nb = getEnums().size();
		int cpt = 1;
		for (String field : getEnums()) {
			tmp.append("\t/** {@link " + getName() + "} " + field + ". */\n");
			tmp.append("\t" + field).append(cpt++ < nb ? "," : ";").append("\n");
		}
		tmp.append("\n");
		tmp.append("}\n");

		return tmp.toString();
	}

	/**
	 * write class to folder.
	 *
	 * @param folder
	 *            String destination folder
	 * @throws IOException
	 *             if error occurs
	 */
	public void write(final String folder) throws IOException {

		try (FileWriter fw =
				new FileWriter(new File(folder + File.separator + getPackageName().replace(".", File.separator)
						+ File.separator + getName() + ".java"))) {
			fw.write(getJavaFile());
			fw.flush();
		}
	}

	/**
	 * get Test instruction.
	 *
	 * @param tab
	 *            the tab car
	 * @return the test instruction
	 */
	public String getTest() {
		return getPackageName() + "." + getName() + "." + getEnumTest();
	}

	/**
	 * getEnumTest.
	 *
	 * @return the enum test.
	 */
	public String getEnumTest() {
		return getEnums().get(0);
	}

}
