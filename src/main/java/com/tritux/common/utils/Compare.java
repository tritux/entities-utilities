package com.tritux.common.utils;

import static com.tritux.common.utils.Equals.isEquals;

import java.util.Collection;
import java.util.Map;


/**
 * A utility that makes comparing 2 comparable objects easier
 * @author Tritux
 *
 */
public class Compare implements Comparable<Compare>{


	/** {@link Compare} GREATER. */
	public static final Compare GREATER = new Compare(1);

	/** {@link Compare} EQUALS. */
	public static final Compare EQUALS = new Compare(0);

	/** {@link Compare} LOWER. */
	public static final Compare LOWER = new Compare(-1);

	/** {@link int} compare. */
	private final int compare;

	/**
	 * Constructor.
	 *
	 * @param compare
	 *            int co;pqre
	 */
	private Compare(final int compare) {
		this.compare = compare;
	}

	/**
	 * Constructor.
	 *
	 * @param <T>
	 *            the class object to compare.
	 * @param o1
	 *            first Object to compare.
	 * @param o2
	 *            second Object to compare.
	 *
	 */
	public <T> Compare(final Comparable<T> o1, final T o2) {
		if (isEquals(o1, o2)) {
			compare = 0;
		} else {
			if (o1 == null || o2 == null) {
				compare = -1;
			} else {
				compare = o1.compareTo(o2);
			}
		}
	}

	/**
	 * compare value of this Compare.
	 *
	 * @return -1, 0 or 1 according to the compare rules.
	 */
	public int compare() {
		return compare;
	}

	/**
	 * Compare this with other Compare Object, the object are compared only if this is Equals Compare.
	 *
	 * @param <T>
	 *            the class object to compare.
	 * @param o1
	 *            first Object to compare.
	 * @param o2
	 *            second Object to compare.
	 * @return the new Compare Object.
	 */
	public <T> Compare andCompare(final Comparable<T> o1, final T o2) {
		if (compare() != 0) {
			return this;
		}
		return new Compare(o1, o2);
	}

	/**
	 * Compare this with other Compare Object, the object are compared only if this is Equals Compare.
	 *
	 * @param <T>
	 *            key class of map
	 * @param <U>
	 *            value class of map
	 * @param o1
	 *            first Object to compare.
	 * @param o2
	 *            second Object to compare.
	 * @return the new Compare Object.
	 */
	public <T, U> Compare andCompare(final Map<Comparable<T>, Comparable<U>> o1, final Map<T, U> o2) {
		if (compare() != 0) {
			return this;
		}
		// if equals, return EQUALS
		if (isEquals(o1, o2)) {
			return EQUALS;
		}
		// check size to compare list
		if (o1.size() > o2.size()) {
			return GREATER;
		}
		// return LOWER
		return LOWER;
	}

	/**
	 * Compare this with other Compare Object, the object are compared only if this is Equals Compare.
	 *
	 * @param o1
	 *            first Object to compare.
	 * @param o2
	 *            second Object to compare.
	 * @return the new Compare Object.
	 */
	public Compare andCompare(final byte[] o1, final byte[] o2) {
		if (compare() != 0) {
			return this;
		}
		// if equals, return EQUALS
		if (isEquals(o1, o2)) {
			return EQUALS;
		}
		// check size to compare list
		if (o1.length > o2.length) {
			return GREATER;
		}
		// return LOWER
		return LOWER;
	}

	/**
	 * Compare this with other Compare Object, the object are compared only if this is Equals Compare.
	 *
	 * @param <T>
	 *            the class object to compare.
	 * @param o1
	 *            first Object to compare.
	 * @param o2
	 *            second Object to compare.
	 * @return the new Compare Object.
	 */
	public <T extends Comparable<T>> Compare andCompare(final Collection<T> o1, final Collection<T> o2) {
		if (compare() != 0) {
			return this;
		}
		// if equals, return EQUALS
		if (isEquals(o1, o2)) {
			return EQUALS;
		}
		// check size to compare list
		if (o1.size() > o2.size()) {
			return GREATER;
		}
		// return LOWER
		return LOWER;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Compare)) {
			return false;
		}
		return new Equals(compare(), ((Compare) obj).compare()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCode(compare()).hashCode();
	}

	@Override
	public String toString() {
		return String.valueOf(compare());
	}

	@Override
	public int compareTo(final Compare o) {
		return Integer.compare(compare(), o.compare());
	}

}
