package com.tritux.common.utils;

import static com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;

/**
 * Allows to serialize and deserialize JSON data from various input sources.
 * 
 * @author Tritux
 * 
 */
public class JsonUtils {

	/** {@link ObjectWriter} JSON_WRITER. */
	private static final ObjectMapper JSON_MAPPER = new ObjectMapper().registerModule(new AfterburnerModule());

	/** {@link ObjectWriter} JSON_WRITER. */
	private static final ObjectWriter JSON_WRITER = JSON_MAPPER.writer();

	/** {@link ObjectWriter} PRETTY_JSON_WRITER. */
	private static final ObjectWriter PRETTY_JSON_WRITER = JSON_MAPPER.enable(INDENT_OUTPUT).writer();

	/**
	 * deprecated protected constructor.
	 * 
	 * @deprecated
	 */
	@Deprecated
	protected JsonUtils() {
		super();
	}

	/**
	 * convert a body input stream into an object.
	 * <p>
	 * the stream must be a JSon stream.
	 * 
	 * @param <T>
	 *            the return type object class.
	 * @param trame
	 *            the string to read.
	 * @param returnType
	 *            the return type object class.
	 * @return the converted object or null if error occurs.
	 * @throws IOException
	 *             if error occurs.
	 */
	public static <T> T fromJson(final String trame, final Class<T> returnType) throws IOException {
		return JSON_MAPPER.readerFor(returnType).readValue(trame);
	}

	/**
	 * convert a body input stream into an object.
	 * <p>
	 * the stream must be a JSon stream.
	 * 
	 * @param <T>
	 *            the return type object class.
	 * @param trame
	 *            the string to read.
	 * @param returnType
	 *            the return {@link TypeReference} object class.
	 * @return the converted object or null if error occurs.
	 * @throws IOException
	 *             if error occurs.
	 */
	public static <T> T fromJson(final String trame, final TypeReference<T> returnType) throws IOException {
		return JSON_MAPPER.readerFor(returnType).readValue(trame);
	}

	/**
	 * convert a body input stream into an object.
	 * <p>
	 * the stream must be a JSon stream.
	 * 
	 * @param <T>
	 *            the return type object class.
	 * @param trame
	 *            the string to read.
	 * @param returnType
	 *            the return type object class.
	 * @return the converted object or null if error occurs.
	 * @throws IOException
	 *             if error occurs.
	 */
	@SuppressWarnings("deprecation")
	public static <T> T fromJson(final InputStream trame, final Class<T> returnType) throws IOException {
		return JSON_MAPPER.reader(returnType).readValue(trame);
	}

	/**
	 * convert a body input stream into an object.
	 * <p>
	 * the stream must be a JSon stream.
	 * 
	 * @param <T>
	 *            the return type object class.
	 * @param trame
	 *            the string to read.
	 * @param returnType
	 *            the return {@link TypeReference} object class.
	 * @return the converted object or null if error occurs.
	 * @throws IOException
	 *             if error occurs.
	 */
	@SuppressWarnings("deprecation")
	public static <T> T fromJson(final InputStream trame, final TypeReference<T> returnType) throws IOException {
		return JSON_MAPPER.reader(returnType).readValue(trame);
	}

	/**
	 * convert a java object to json string.
	 * 
	 * @param <T>
	 *            The input object Type
	 * @param object
	 *            the input object
	 * @return the json string of the object
	 * @throws JsonProcessingException
	 *             if error occurs.
	 */
	public static <T> String toJson(final T object) throws JsonProcessingException {
		return JSON_WRITER.writeValueAsString(object);
	}

	/**
	 * convert a java object to pretty json string.
	 * 
	 * @param <T>
	 *            The input object Type
	 * @param object
	 *            the input object
	 * @return the pretty json string of the object
	 * @throws JsonProcessingException
	 *             if error occurs.
	 */
	public static <T> String toPrettyJson(final T object) throws JsonProcessingException {
		return PRETTY_JSON_WRITER.writeValueAsString(object);
	}

	/**
	 * test if object can be serialized and deserialized by Json.
	 *
	 * @param <T>
	 *            the Object class to serialize in json.
	 * @param source
	 *            the object to test.
	 * @return true if object can be serialized and de-serialized by a JSon parser (result object is equals to source
	 *         object).
	 */
	@SuppressWarnings("unchecked")
	public static <T> boolean canBeJSonSerialized(final T source) {
		try {
			return Equals.isEquals(source, (T) fromJson(toJson(source), source.getClass()));
		} catch (IOException e) {
			// log exception.
			return false;
		}
	}

	/**
	 * test if object can be serialized and deserialized by Json.
	 *
	 * @param source
	 *            the object to test.
	 * @return true if object can be serialized and de-serialized by a JSon parser (result object is equals to source
	 *         object).
	 */
	public static boolean canBeJSonNullDeserialized(final Class<?> source) {
		try {
			return null == fromJson("\"\"", source);
		} catch (IOException e) {
			// log exception.
			return false;
		}
	}
}
