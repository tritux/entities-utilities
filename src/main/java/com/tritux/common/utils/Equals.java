package com.tritux.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Invariant class defined to check equality between 2 object.
 *
 * possibility to make link equality with current.
 *
 * @author Tritux
 *
 */
public final class Equals {

	/**
	 * always false value Equals.
	 */
	public static final Equals FALSE = new Equals(false);
	/**
	 * always true value Equals.
	 */
	public static final Equals TRUE = new Equals(true);

	/**
	 * boolean equals.
	 */
	private final boolean equals;

	/**
	 * Constructor.
	 *
	 * @param equals
	 *            the equality to set.
	 */
	protected Equals(final boolean equals) {
		this.equals = equals;
	}

	/**
	 * Constructor.
	 *
	 * check if 2 object are equals.
	 *
	 * @param <T>
	 *            class type of tested objects.
	 * @param obj1
	 *            first object.
	 * @param obj2
	 *            second object.
	 */
	public <T> Equals(final T obj1, final T obj2) {
		equals = isEquals(obj1, obj2);
	}

	/**
	 * Constructor.
	 *
	 * check if 2 byte[] are equals.
	 *
	 * @param obj1
	 *            first byte[].
	 * @param obj2
	 *            second byte[].
	 */
	public Equals(final byte[] obj1, final byte[] obj2) {
		equals = isEquals(obj1, obj2);
	}

	/**
	 * AND operator.
	 *
	 * check if the 2 object are equals and make an AND with current.
	 *
	 * @param <T>
	 *            class type of tested objects.
	 * @param obj1
	 *            first object.
	 * @param obj2
	 *            second object.
	 * @return the equals regarding to AND rules.
	 */
	public <T> Equals andEquals(final T obj1, final T obj2) {
		if (isEquals() && isEquals(obj1, obj2)) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * AND operator.
	 *
	 * check if the 2 byte[] are equals and make an AND with current.
	 *
	 * @param obj1
	 *            byte[] first object.
	 * @param obj2
	 *            byte[] second object.
	 * @return the equals regarding to AND rules.
	 */
	public Equals andEquals(final byte[] obj1, final byte[] obj2) {
		if (isEquals() && isEquals(obj1, obj2)) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * AND operator.
	 *
	 * make an AND with current.
	 *
	 * @param bool
	 *            boolean to make AND with current.
	 * @return the equals regarding to AND rules.
	 */
	public Equals and(final boolean bool) {
		if (isEquals() && bool) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * OR operator.
	 *
	 * check if the 2 objects are equals and make an OR with current.
	 *
	 * @param <T>
	 *            class type of tested objects.
	 * @param obj1
	 *            first object.
	 * @param obj2
	 *            second object.
	 * @return the equals regarding to OR rules.
	 */
	public <T> Equals orEquals(final T obj1, final T obj2) {
		if (isEquals() || isEquals(obj1, obj2)) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * OR operator.
	 *
	 * make an OR with current.
	 *
	 * @param bool
	 *            boolean to make OR with current.
	 * @return the equals regarding to OR rules.
	 */
	public Equals or(final boolean bool) {
		if (isEquals() || bool) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * XOR operator.
	 *
	 * check if the 2 objects are equals and make an XOR with current.
	 *
	 * @param <T>
	 *            class type of tested objects.
	 * @param obj1
	 *            first object.
	 * @param obj2
	 *            second object.
	 * @return the equals regarding to XOR rules.
	 */
	public <T> Equals xorEquals(final T obj1, final T obj2) {
		if (isEquals() ^ isEquals(obj1, obj2)) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * XOR operator.
	 *
	 * make an XOR with current.
	 *
	 * @param bool
	 *            boolean to make XOR with current.
	 * @return the equals regarding to XOR rules.
	 */
	public Equals xor(final boolean bool) {
		if (isEquals() ^ bool) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * get NOT operator to current.
	 *
	 * @return false if current is true, and true if current is false.
	 */
	public Equals not() {
		if (isEquals()) {
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * getter of equals.
	 *
	 * @return the equals
	 */
	public boolean isEquals() {
		return equals;
	}

	/**
	 * check if the two parameters are equals.
	 * <p>
	 * same result as new Equals(o1,o2).isEquals()
	 *
	 * @param <T>
	 *            class type of tested objects.
	 * @param o1
	 *            first parameter
	 * @param o2
	 *            second parameter
	 * @return true if the parameters are equals.
	 */
	public static <T> boolean isEquals(final T o1, final T o2) {
		// if both are null return true
		if (o1 == null && o2 == null) {
			return true;
		}
		// if one of them is null, but not the other, return false
		if (o1 == null || o2 == null) {
			return false;
		}
		// if it the same object
		if (o1 == o2) {
			return true;
		}
		// if is array, check equals on each members.
		if (o1.getClass().isArray()) {
			return Arrays.equals((Object[]) o1, (Object[]) o2);
		}
		if (o1 instanceof Collection) {
			return new ArrayList<>((Collection<?>) o1).equals(new ArrayList<>((Collection<?>) o2));
		}
		// check if equals
		return o1.equals(o2);
	}

	/**
	 * check if the two byte[] parameters are equals.
	 * <p>
	 * same result as new Equals(o1,o2).isEquals()
	 *
	 * @param o1
	 *            byte[] first parameter
	 * @param o2
	 *            byte[] second parameter
	 * @return true if the parameters are equals.
	 */
	public static boolean isEquals(final byte[] o1, final byte[] o2) {
		// if both are null return true
		if (o1 == null && o2 == null) {
			return true;
		}
		// if one of them is null, but not the other, return false
		if (o1 == null || o2 == null) {
			return false;
		}
		// if it the same object
		if (o1 == o2) {
			return true;
		}
		// check equals on each members.
		return Arrays.equals(o1, o2);
	}

	@Override
	public String toString() {
		return String.valueOf(isEquals());
	}

	@Override
	public int hashCode() {
		return new HashCode(isEquals()).hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Equals)) {
			return false;
		}
		Equals tmp = (Equals) obj;
		if (isEquals()) {
			return tmp.isEquals();
		}
		return !tmp.isEquals();
	}
}
