package com.tritux.common.utils;


import static com.tritux.common.utils.Null.isNullOrEmpty;

import java.util.Arrays;

/**
 * HashCode builder.
 * 
 * @author Tritux
 * 
 */
public class HashCode {

	private static final int N_31 = 31;
	/**
	 * value of hash code.
	 */
	private final int hashCode;

	/**
	 * Constructor.
	 * 
	 * @param <T>
	 *            class type of tested objects.
	 * @param o
	 *            the object to hash.
	 */
	public <T> HashCode(final T o) {
		this(isNullOrEmpty(o) ? 0 : o.getClass().isArray() ? Arrays.hashCode((Object[]) o) : o.hashCode());
	}

	/**
	 * private Constructor.
	 * 
	 * @param h
	 *            the value of hash to set.
	 */
	private HashCode(final int h) {
		hashCode = h;
	}

	/**
	 * compute a new hash code with current and return the new HashCode object.
	 * 
	 * @param <T>
	 *            class type of tested objects.
	 * @param o
	 *            the object to compute.
	 * @return the new HashCode builder.
	 */
	public <T> HashCode append(final T o) {
		if (isNullOrEmpty(o)) {
			return this;
		}
		if (o.getClass().isArray()) {
			return new HashCode(hashCode() * N_31 + Arrays.hashCode((Object[]) o));
		}
		return new HashCode(hashCode() * N_31 + o.hashCode());
	}

	/**
	 * compute a new hash code with current and return the new HashCode object.
	 * 
	 * @param o
	 *            byte[] the object to compute.
	 * @return the new HashCode builder.
	 */
	public HashCode append(final byte[] o) {
		if (isNullOrEmpty(o)) {
			return this;
		}
		return new HashCode(hashCode() * N_31 + Arrays.hashCode(o));
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof HashCode)) {
			return false;
		}
		HashCode tmp = (HashCode) obj;
		return hashCode == tmp.hashCode;
	}

	@Override
	public int hashCode() {
		return hashCode;
	}
}
