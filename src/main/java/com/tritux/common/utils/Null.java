package com.tritux.common.utils;

import java.util.Arrays;
import java.util.Collection;

import com.tritux.common.exception.NotFoundArgumentException;
import com.tritux.common.exception.NullArgumentException;

/**
 * Some utilities to perform various null-checks on an input argument.
 * @author Tritux
 *
 */
public class Null {


	/**
	 * deprecated protected constructor.
	 * 
	 * @deprecated
	 */
	@Deprecated
	protected Null() {
		super();
	}

	/**
	 * check is the parameter is null or empty.
	 * 
	 * @param <T>
	 *            class type of tested objects.
	 * @param o
	 *            parameter to check.
	 * @return true is the parameter is null or empty, false otherwise.
	 */
	public static <T> Boolean isNullOrEmpty(final T o) {
		if (o == null) {
			return true;
		}
		if (o instanceof String) {
			return "".equals(((String) o).trim());
		}
		if (o.getClass().isArray()) {
			return Arrays.asList((Object[]) o).isEmpty();
		}
		if (o instanceof Collection<?>) {
			return ((Collection<?>) o).isEmpty();
		}
		return false;
	}

	/**
	 * check is the parameter (byte array) is null or empty.
	 * 
	 * @param o
	 *            byte[] parameter to check.
	 * @return true is the parameter is null or empty, false otherwise.
	 */
	public static Boolean isNullOrEmpty(final byte[] o) {
		if (o == null) {
			return true;
		}
		return o.length == 0;
	}

	/**
	 * check if the parameter is null.
	 * 
	 * throw an IllegalArgumentException if the object is null or empty.
	 * 
	 * @param <T>
	 *            class type of tested objects.
	 * @param variableName
	 *            the variable name.
	 * @param o
	 *            the parameter to check.
	 */
	public static <T> void checkNotNull(final String variableName, final T o) {
		if (o == null) {
			throw new NullArgumentException(variableName, variableName + " is null");
		}
	}

	/**
	 * check if the parameter is null.
	 * 
	 * throw an NotFoundArgumentException if the object is null or empty.
	 * 
	 * @param <T>
	 *            class type of tested objects.
	 * @param variableName
	 *            the variable name.
	 * @param o
	 *            the parameter to check.
	 */
	public static <T, U> void checkExist(final String variableName, final T o, final U identifier) {
		if (isNullOrEmpty(o)) {
			throw new NotFoundArgumentException(variableName, String.valueOf(identifier));
		}
	}

	/**
	 * check if the parameter is null or empty.
	 * 
	 * throw an IllegalArgumentException if the object is null or empty.
	 * 
	 * @param <T>
	 *            class type of tested objects.
	 * @param variableName
	 *            the variable name.
	 * @param o
	 *            the parameter to check.
	 */
	public static <T> void checkNotNullOrEmpty(final String variableName, final T o) {
		if (isNullOrEmpty(o)) {
			throw new NullArgumentException(variableName, variableName + " is null");
		}
	}


}
