package com.tritux.common.utils;

import static com.tritux.common.utils.Null.isNullOrEmpty;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
import static org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.StringTokenizer;

import com.tritux.common.exception.InvalidArgumentException;


/**
 * Define some useful utilities.
 * 
 * @author Tritux
 * 
 */
public final class Utils {

	/**
	 * generic String to normalize.
	 */
	private static final String[][] NORMALIZED_STRINGS = new String[][]{
		// appostrophe manquante
		{ " c ([aAeEiIoOuU])", " c'$1" },
		{ " d ([aAeEiIoOuU])", " d'$1" },
		{ " j ([aAeEiIoOuU])", " j'$1" },
		{ " l ([aAeEiIoOuU])", " l'$1" },
		{ " m ([aAeEiIoOuU])", " m'$1" },
		{ " n ([aAeEiIoOuU])", " n'$1" },
		{ "qu ([aAeEiIoOuU])", "qu'$1" },
		{ " s ([aAeEiIoOuU])", " s'$1" },
		// articles
		{ " Les ", " les " },
		{ " Des ", " des " },
		{ " Une ", " une " },
		// cas des acronymes les plus courant
		{ " bp ", " BP " }, { "^bp ", "BP " }, { " zi ", " ZI " }, { "^zi ", "ZI " }, { " za ", " ZA " },
		{ "^za ", "ZA " }, { " st ", " Saint " }, { "^st ", "Saint " },
		{ "^St-", "Saint-" },
		{ " Ste ", " Sainte " },
		{ "^Ste ", "Sainte " },
		{ "^Ste-", "Sainte-" },
		{ " Sarl ", " SARL " },
		{ "^Sarl ", "SARL " },
		{ " Sarl$", " SARL" },
		{ " Lez ", " lez " },
		{ "-Lez-", "-lez-" },
		// chiffres romains
		{ " i ", " I " }, { " i$", " I" }, { " ii ", " II " }, { " ii$", " II" }, { " Iii ", " III " },
		{ " Iii$", " III" }, { " iv ", " IV " }, { " iv$", " IV" }, { " v ", " V " }, { " v$", " V" },
		{ " vi ", " VI " }, { " vi$", " VI" }, { " Vii ", " VII " }, { " Vii$", " VII" }, { " Viii ", " VIII " },
		{ " Viii$", " VIII" }, { " ix ", " IX " }, { " ix$", " IX" }, { " x ", " X " }, { " x$", " X" },
		{ " xi ", " XI " }, { " xi$", " XI" }, { " Xii ", " XII " }, { " Xii$", " XII" }, { " Xiii ", " XIII " },
		{ " Xiii$", " XIII" }, { " Xiv ", " XIV " }, { " Xiv$", " XIV" }, { " xv ", " XV " }, { " xv$", " XV" },
		{ " Xvi ", " XVI " }, { " Xvi$", " XVI" }, { " Xvii ", " XVII " }, { " Xvii$", " XVII" },
		{ " Xviii ", " XVIII " }, { " Xviii$", " XVIII" }, { " Xix ", " XIX " }, { " Xix$", " XIX" },
		{ " xx ", " XX " }, { " xx$", " XX" } };
	private static final int N_1024 = 1024;

	/**
	 * 
	 * Constructor.
	 * 
	 */
	protected Utils() {
		super();
	}

	/**
	 * normalize a text string.
	 * 
	 * the normalize will consist in : <br/>
	 * - to lower case <br/>
	 * - delete some unwanted character (apostrophe from world, anti slash ...) <br/>
	 * - delete double spaces <br/>
	 * - upper case the first letter of each distinct word (only for word composed by 3 or more letter) <br/>
	 * - upper case the first letter from multiple world.<br/>
	 * - trim the string <br/>
	 * 
	 * @param texte
	 *            the string to normalize.
	 * @return the normalized String.
	 */
	public static final String normalise(final String texte) {
		if (isNullOrEmpty(texte)) {
			return null;
		}
		String tmp = texte.toLowerCase();
		tmp = tmp.replace("’", "'");
		while (tmp.indexOf("  ") != -1) {
			tmp = tmp.replaceAll("  ", " ");
		}
		tmp = tmp.trim();

		// mise en majuscule
		String normalise = capitalize(tmp);

		// normalization des chaines les plus courrantes
		for (String[] normalizedString : NORMALIZED_STRINGS) {
			normalise = normalise.replaceAll(normalizedString[0], normalizedString[1]);
		}

		return normalise.trim();
	}

	/**
	 * Escape the characters in a String using HTML4 entities.
	 * 
	 * @param texte
	 *            the texte
	 * @return the string
	 */
	public static final String escapeHtml(final String texte) {
		return escapeHtml4(texte);
	}

	/**
	 * Unescape a string containing html entity escapes .
	 * 
	 * @param texte
	 *            the texte
	 * @return the string
	 */
	public static final String unEscapeHtml(final String texte) {
		return unescapeHtml4(texte);
	}

	/**
	 * encode String for use in URL.
	 * 
	 * @param urlPart
	 *            the string to encode.
	 * @return the encoded String in ISO-8859-15
	 */
	public static String urlEncode(final String urlPart) {
		try {
			return URLEncoder.encode(urlPart, "ISO-8859-15");
		} catch (UnsupportedEncodingException e) {
			return urlPart;
		}
	}

	/**
	 * encode String for use in URL.
	 *
	 * @param urlPart
	 *            the string to encode.
	 * @return the encoded String in ISO-8859-15
	 */
	public static String urlDecode(final String urlPart) {
		try {
			return URLDecoder.decode(urlPart, "ISO-8859-15");
		} catch (UnsupportedEncodingException e) {
			return urlPart;
		}
	}

	/**
	 * 
	 * capitalize the first letter of 3 or more character String.
	 * 
	 * @param text
	 *            the text to capitalize.
	 * @return the capitalized text.
	 */
	private static final String capitalize(final String text) {
		StringBuilder capitalize = new StringBuilder(text.length());

		// mise de la premiére lettre en majuscule apres un espace
		StringTokenizer tk = new StringTokenizer(text, " ");
		while (tk.hasMoreElements()) {
			String tkTmp = tk.nextToken();
			// uniquement si plus de 2 caractères
			if (tkTmp.length() > 2) {
				// si contient - ou . alors chaque mot a une majuscule
				if (tkTmp.contains("-")) {
					capitalize.append(" ").append(capitalize(tkTmp, "-"));
				} else if (tkTmp.contains(".")) {
					capitalize.append(" ").append(capitalize(tkTmp, "."));
				} else {
					capitalize.append(" ").append(tkTmp.substring(0, 1).toUpperCase()).append(tkTmp.substring(1));
				}
			} else {
				capitalize.append(" ").append(tkTmp);
			}
		}
		return capitalize.toString().trim();
	}

	/**
	 * capitalize the first letter of word with separator.
	 * 
	 * @param text
	 *            the text to capitalize.
	 * @param separator
	 *            the separator to use.
	 * @return the capitalized text.
	 */
	private static final String capitalize(final String text, final String separator) {
		StringBuilder capitalize = new StringBuilder(text.length());
		StringTokenizer tk2 = new StringTokenizer(text, separator);
		int i = 0;
		while (tk2.hasMoreElements()) {
			if (i++ != 0) {
				capitalize.append(separator);
			}
			String tkTmp2 = tk2.nextToken();
			capitalize.append(tkTmp2.substring(0, 1).toUpperCase()).append(tkTmp2.substring(1));
		}
		// si fini par le separator, le tokenizer l'a supprimé, on le rajoute
		if (text.endsWith(separator)) {
			capitalize.append(separator);
		}
		return capitalize.toString();
	}

	/**
	 * read data from and InputStream.
	 *
	 * @param is
	 *            InputStream to read.
	 * @return the byte[] read from the inputStream.
	 */
	public static byte[] readFromStream(final InputStream is) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
			int lue = 0;
			byte[] tmp = new byte[N_1024 * N_1024];
			while ((lue = is.read(tmp)) != -1) {
				baos.write(tmp, 0, lue);
			}
			return baos.toByteArray();
		} catch (IOException ioe) {
			return new byte[0];
		}
	}

	/**
	 * assert the the class has an empty default protected constructor, define hashCode, equals and toString method.
	 *
	 * @param <T>
	 *            the class to test.
	 * @param classToTest
	 *            the class to test.
	 * @param sampleObject
	 *            sample object to test
	 * @return true, if the class has an empty default protected constructor, false otherwise.
	 */
	@SuppressWarnings("unchecked")
	public static <T> boolean hasProtectedEmptyConstructorEqualsAndHashCodeAndCompareTo(final Class<T> classToTest,
			final Comparable<T> sampleObject) {

		T cloneSampleObject = null;
		try {
			// get the constructor that takes no parameters
			Constructor<?> constructor = classToTest.getDeclaredConstructor((Class<?>[]) null);

			// the modifiers int can tell us metadata about the constructor
			int constructorModifiers = constructor.getModifiers();

			// but we're only interested in knowing that it's private
			if (!Modifier.isProtected(constructorModifiers)) {
				throw new InvalidArgumentException(classToTest.getName(), "constructor is not protected");
			}

			// constructor is private so we first have to make it accessible
			constructor.setAccessible(true);

			// now construct a new instance of object, and clone sample object to test equals
			cloneSampleObject = (T) constructor.newInstance((Object[]) null);
			// set values for clone object
			for (Field field : classToTest.getDeclaredFields()) {
				field.setAccessible(true);
				if (!Modifier.isStatic(field.getModifiers())) {
					field.set(cloneSampleObject, field.get(sampleObject));
				}
			}
		} catch (Exception e) {
			throw new InvalidArgumentException(classToTest.getName(), "missing protected constructor");
		}
		try {
			classToTest.getDeclaredMethod("equals", new Class<?>[]{ Object.class });
			Method hashCode = classToTest.getDeclaredMethod("hashCode", (Class<?>[]) null);
			hashCode.invoke(sampleObject);
		} catch (Exception e) {
			throw new InvalidArgumentException(classToTest.getName(), "missing equals or hashCode method");
		}
		// equality between the sample object and its clone
		if (!sampleObject.equals(cloneSampleObject)) {
			throw new InvalidArgumentException(classToTest.getName(), "sample objects not equals");
		}
		// test equality over other objects (like Integer for example)
		if (sampleObject.equals(Integer.MAX_VALUE)) {
			return false;
		}
		// toString declaration
		try {
			Method toString = classToTest.getDeclaredMethod("toString", (Class<?>[]) null);
			toString.invoke(sampleObject);
		} catch (Exception e) {
			throw new InvalidArgumentException(classToTest.getName(), "missing toString method");
		}
		// compare method
		if (0 != sampleObject.compareTo((T) sampleObject)) {
			throw new InvalidArgumentException(classToTest.getName(), "bad compare method");
		}
		return true;
	}

	/**
	 * assert the the class has an empty default protected constructor.
	 *
	 * @param classToTest
	 *            the class to test.
	 * @return true, if the class has an empty default protected constructor, false otherwise.
	 */
	public static boolean hasProtectedEmptyConstructor(final Class<?> classToTest) {
		try {
			// get the constructor that takes no parameters
			Constructor<?> constructor = classToTest.getDeclaredConstructor((Class<?>[]) null);

			// the modifiers int can tell us metadata about the constructor
			int constructorModifiers = constructor.getModifiers();

			// but we're only interested in knowing that it's private
			if (!Modifier.isProtected(constructorModifiers)) {
				throw new InvalidArgumentException(classToTest.getName(), "not protected constructor");
			}

			// constructor is private so we first have to make it accessible
			constructor.setAccessible(true);

			// now construct an instance
			constructor.newInstance((Object[]) null);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
