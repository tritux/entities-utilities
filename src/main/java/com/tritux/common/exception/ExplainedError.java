package com.tritux.common.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tritux.common.utils.JsonUtils;

/**
 * Will be used as an exception argument for a rich explanation
 *
 * @author Tritux
 *
 */
public class ExplainedError {

	/** {@link String} type. */
	private final String type;
	/** {@link String} object. */
	private final String object;
	/** {@link String} explain. */
	private final String explain;

	/**
	 * Constructor.
	 *
	 * @param type
	 *            String type to set
	 * @param object
	 *            String object to set
	 * @param explain
	 *            String explain to set
	 */
	public ExplainedError(final String type, final String object, final String explain) {
		super();
		this.type = type;
		this.object = object;
		this.explain = explain;
	}

	/**
	 * Getter of type.
	 *
	 * @return {@link String} the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Getter of object.
	 *
	 * @return {@link String} the object
	 */
	public String getObject() {
		return object;
	}

	/**
	 * Getter of explain.
	 *
	 * @return {@link String} the explain
	 */
	public String getExplain() {
		return explain;
	}

	@Override
	public String toString() {
		try {
			return JsonUtils.toJson(this);
		} catch (JsonProcessingException jpe) {
			return "{ error : { type : \"" + getType() + "\", object : \"" + getObject() + "\", explain :\""
					+ getExplain().replace("\"", "\\\"") + "\" } }";
		}
	}

}
