//package com.tritux.common.exception;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.tritux.common.valueObject.address.URL;
//import com.tritux.common.valueObject.utils.JSon;
//
///**
// * Message customis�e pour les methodes de controlle annot�e avec @ExceptionHandler qui g�rent les cas d'erreur.
// * 
// * @author Tritux
// * 
// */
//public class InfoErreur {
//
//	/**
//	 * l'url de service REST.
//	 */
//	private final URL url;
//
//	/**
//	 * code d'erreur.
//	 */
//	private final ErrorCode errorCode;
//
//	/**
//	 * message d'erreur.
//	 */
//	private final String errorMessage;
//
//	/**
//	 * Default protected Constructor for {@link InfoErreur}.
//	 */
//	protected InfoErreur() {
//		this.errorCode = null;
//		this.url = null;
//		this.errorMessage = null;
//	}
//
//	/**
//	 * Constructor.
//	 * 
//	 * @param builder
//	 *            {@link Builder}
//	 */
//	protected InfoErreur(final Builder builder) {
//		this.url = builder.getUrl();
//		this.errorMessage = builder.getErrorMessage();
//		this.errorCode = builder.getErrorCode();
//	}
//
//	/**
//	 * get a new {@link Builder} for {@link InfoErreur}.
//	 * 
//	 * @return a new {@link Builder} for {@link InfoErreur}.
//	 */
//	public static Builder builder() {
//		return new Builder();
//	}
//
//	/**
//	 * return {@link String} content of {@link InfoErreur}.
//	 */
//	@Override
//	public String toString() {
//		try {
//			return JSon.toPrettyJson(this);
//		} catch (JsonProcessingException e) {
//			return super.toString();
//		}
//	}
//
//	/**
//	 * @return {@link #URL}.
//	 */
//	public String getUrl() {
//		return url.getURL();
//	}
//
//	/**
//	 * 
//	 * @return {@link #String}.
//	 */
//	public String getErrorCode() {
//		return errorCode.getErrorCode();
//	}
//
//	/**
//	 * @return {@link #String}.
//	 */
//	public String getErrorMessage() {
//		return errorMessage;
//	}
//
//	/**
//	 * Builder of {@link #InfoErreur}
//	 * 
//	 * @author Amira Abdellatif
//	 * 
//	 */
//	public static class Builder {
//
//		/**
//		 * l'url de service REST.
//		 */
//		private URL url;
//
//		/**
//		 * code d'erreur.
//		 */
//		private ErrorCode errorCode;
//
//		/**
//		 * Getter of errorMessage.
//		 * 
//		 * @return {@link #String}.
//		 */
//		private String errorMessage;
//
//		/**
//		 * 
//		 * @param url
//		 *            .
//		 * 
//		 * @return {@link #Builder}.
//		 */
//		public Builder url(String url) {
//			this.url = URL.build(url);
//			return this;
//		}
//
//		/**
//		 * 
//		 * @param errorCode
//		 *            .
//		 * 
//		 * @return {@link #Builder}.
//		 */
//		public Builder errorCode(String errorCode) {
//			this.errorCode = ErrorCode.build(errorCode);
//			return this;
//		}
//
//		/**
//		 * 
//		 * @param errorMessage
//		 *            .
//		 * @return {@link #Builder}.
//		 */
//		public Builder errorMessage(String errorMessage) {
//			this.errorMessage = errorMessage;
//			return this;
//		}
//
//		/**
//		 * build the {@link InfoErreur}.
//		 * 
//		 * @return the builded {@link InfoErreur}.
//		 */
//		public InfoErreur build() {
//			return new InfoErreur(this);
//		}
//
//		/**
//		 * Getter of url.
//		 * 
//		 * @return {@link #URL}.
//		 */
//		public URL getUrl() {
//			return url;
//		}
//
//		/**
//		 * Getter of errorMessage.
//		 * 
//		 * @return {@link #String}.
//		 */
//		public String getErrorMessage() {
//			return errorMessage;
//		}
//
//		/**
//		 * Getter of errorCode.
//		 * 
//		 * @return {@link #ErrorCode}.
//		 */
//		public ErrorCode getErrorCode() {
//			return errorCode;
//		}
//
//	}
//}
