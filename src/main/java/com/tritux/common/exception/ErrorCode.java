package com.tritux.common.exception;


import static com.tritux.common.utils.Null.checkNotNullOrEmpty;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tritux.common.utils.Compare;
import com.tritux.common.utils.Equals;
import com.tritux.common.utils.HashCode;
import com.tritux.common.utils.JsonUtils;

/**
 * Define the ErrorCode (reference du code erreur) used by tritux:<br>
 * an ErroreCode is a numbers separated by points. 2 Format are expected <br>
 * <li>X</li> <li>X(.X)*n</li> where x and n are a digital number and "." is a constant.
 * 
 * @author Tritux.
 * 
 */
@Embeddable
public class ErrorCode implements Comparable<ErrorCode> {

	/**
	 * the errorCode.
	 */
	private final String errorCode;

	/**
	 * default protected Constructor, only use for JPA.
	 * 
	 * @deprecated
	 */
	@Deprecated
	protected ErrorCode() {
		super();
		errorCode = null;
	}

	/**
	 * Constructor.
	 * 
	 * @param errorCode
	 *            the errorCode to set.
	 */
	protected ErrorCode(final String errorCode) {
		checkNotNullOrEmpty("errorCode", errorCode);
		this.errorCode = verifyErrorCode(errorCode);
	}

	/**
	 * build new errorCode.
	 * 
	 * @param errorCode
	 *            the errorCode to set.
	 * @return new errorCode
	 */
	public static ErrorCode build(final String errorCode) {
		return new ErrorCode(errorCode);
	}

	/**
	 * Getter of ErrorCode.
	 * 
	 * @return the ErrorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * verify the errorCode format.<br>
	 * <br>
	 * an ErroreCode is a numbers separated by points. <br>
	 * 2 Format are expected : <br>
	 * <li>X</li> <li>X(.X)*n</li> where x and n are a digital number and "." is a constant.
	 * 
	 * @param errorCode
	 *            the errorCode to verify.
	 * @return the verified errorCode.
	 */
	private static String verifyErrorCode(final String errorCode) {

		Pattern pattern = Pattern.compile("^\\d+(\\.\\d+)*$");
		Matcher match = pattern.matcher(errorCode);
		if (match.matches()) {
			return errorCode;
		}
		throw new InvalidArgumentException("ErrorCode", "ErrorCode : " + errorCode + " format is not valid");
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ErrorCode)) {
			return false;
		}
		ErrorCode tmp = (ErrorCode) obj;
		return new Equals(getErrorCode(), tmp.getErrorCode()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCode(getErrorCode()).hashCode();
	}

	@Override
	public String toString() {
		try {
			return JsonUtils.toPrettyJson(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}

	@Override
	public int compareTo(ErrorCode o) {
		return new Compare(getErrorCode(), o.getErrorCode()).compare();
	}
}
