package com.tritux.common.exception;


/**
 * An exception type that will be thrown when one argument is passed as null
 *
 * @author Tritux
 *
 */
public class NullArgumentException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** {@link String} object. */
	private final String object;
	/** {@link String} explain. */
	private final String explain;
	/** {@link Throwable} throwable. */
	private final Throwable throwable;

	/**
	 * Constructor.
	 *
	 * @param object
	 *            String object to set
	 * @param explain
	 *            String explain to set
	 */
	public NullArgumentException(final String object, final String explain) {
		super(new ExplainedError("null", object, explain).toString());
		this.object = object;
		this.explain = explain;
		throwable = null;
	}

	/**
	 * Constructor.
	 *
	 * @param object
	 *            String object to set
	 * @param explain
	 *            String explain to set
	 * @param throwable
	 *            {@link Throwable} throwable to set
	 */
	public NullArgumentException(final String object, final String explain, final Throwable throwable) {
		super(new ExplainedError("null", object, explain).toString(), throwable);
		this.object = object;
		this.explain = explain;
		this.throwable = throwable;
	}

	/**
	 * Getter of object.
	 *
	 * @return {@link String} the object
	 */
	public String getObject() {
		return object;
	}

	/**
	 * Getter of explain.
	 *
	 * @return {@link String} the explain
	 */
	public String getExplain() {
		return explain;
	}

	/**
	 * Getter of throwable.
	 *
	 * @return {@link Throwable} the throwable
	 */
	public Throwable getThrowable() {
		return throwable;
	}

}
