package com.tritux.common.exception;


/**
 * A specific exception type that will be thrown when a method invoked with a missing argument
 *
 * @author Tritux
 *
 */
public class NotFoundArgumentException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** {@link String} object. */
	private final String object;
	/** {@link String} explain. */
	private final String explain;
	/** {@link Throwable} throwable. */
	private final Throwable throwable;

	/**
	 * Constructor.
	 *
	 * @param object
	 *            String object to set
	 * @param explain
	 *            String explain to set
	 */
	public NotFoundArgumentException(final String object, final String identifier) {
		super(
				new ExplainedError("not found", object, object + " reference by " + identifier + " does not exist.").toString());
		this.object = object;
		explain = object + " does not exist.";
		throwable = null;
	}

	/**
	 * Constructor.
	 *
	 * @param object
	 *            String object to set
	 * @param explain
	 *            String explain to set
	 * @param throwable
	 *            Throwable throwable to set
	 */
	public NotFoundArgumentException(final String object, final String explain, final Throwable throwable) {
		super(new ExplainedError("not found", object, explain).toString(), throwable);
		this.object = object;
		this.explain = explain;
		this.throwable = throwable;
	}

	/**
	 * Getter of object.
	 *
	 * @return {@link String} the object
	 */
	public String getObject() {
		return object;
	}

	/**
	 * Getter of explain.
	 *
	 * @return {@link String} the explain
	 */
	public String getExplain() {
		return explain;
	}

	/**
	 * Getter of throwable.
	 *
	 * @return {@link Throwable} the throwable
	 */
	public Throwable getThrowable() {
		return throwable;
	}

}
