package com.tritux.common.exception;

/**
 * A specific exception class that will be thrown when an invalid argument
 * is passed to a method
 *
 * @author Tritux
 *
 */
public class InvalidArgumentException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** {@link String} object. */
	private final String object;
	/** {@link String} explain. */
	private final String explain;
	/** {@link Throwable} throwable. */
	private final Throwable throwable;

	/**
	 * Constructor.
	 *
	 * @param object
	 *            String object to set
	 * @param explain
	 *            String explain to set
	 */
	public InvalidArgumentException(final String object, final String explain) {
		super(new ExplainedError("invalid", object, explain).toString());
		this.object = object;
		this.explain = explain;
		throwable = null;
	}

	/**
	 * Constructor.
	 *
	 * @param object
	 *            String object to set
	 * @param explain
	 *            String explain to set
	 * @param throwable
	 *            Throwable throwable to set
	 */
	public InvalidArgumentException(final String object, final String explain, final Throwable throwable) {
		super(new ExplainedError("invalid", object, explain).toString(), throwable);
		this.object = object;
		this.explain = explain;
		this.throwable = throwable;
	}

	/**
	 * Getter of object.
	 *
	 * @return {@link String} the object
	 */
	public String getObject() {
		return object;
	}

	/**
	 * Getter of explain.
	 *
	 * @return {@link String} the explain
	 */
	public String getExplain() {
		return explain;
	}

	/**
	 * Getter of throwable.
	 *
	 * @return {@link Throwable} the throwable
	 */
	public Throwable getThrowable() {
		return throwable;
	}

}
