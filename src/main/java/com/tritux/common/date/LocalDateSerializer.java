package com.tritux.common.date;

import java.io.IOException;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
/**
 * Json Serializer for {@link LocalDate}.
 * 
 * @author Tritux
 * 
 */
@SuppressWarnings("serial")
public class LocalDateSerializer extends StdSerializer<LocalDate> {

	/** Constructor. */
	public LocalDateSerializer() {
		super(LocalDate.class);
	}

	@Override
	public void serialize(final LocalDate value, final JsonGenerator jgen, final SerializerProvider provider)
			throws IOException {
		jgen.writeString(value.toString());
	}
}
