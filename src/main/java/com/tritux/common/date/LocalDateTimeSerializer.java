package com.tritux.common.date;

import java.io.IOException;

import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Json Serializer for {@link LocalDateTime}.
 * 
 * @author Tritux
 * 
 */
@SuppressWarnings("serial")
public class LocalDateTimeSerializer extends StdSerializer<LocalDateTime> {

	/** Constructor. */
	public LocalDateTimeSerializer() {
		super(LocalDateTime.class);
	}

	@Override
	public void serialize(final LocalDateTime value, final JsonGenerator jgen, final SerializerProvider provider)
			throws IOException {
		jgen.writeString(value.toString());
	}
}
