package com.tritux.common.date;

import static com.tritux.common.utils.Null.isNullOrEmpty;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;

/**
 * Json Deserializer for {@link LocalDateTime}. Allowed format:
 * <p>
 * <ul>
 * <li>Long timestamp(milliSeconds)
 * <li>String timestamp(milliSeconds)
 * <li>String {@link LocalDate} like "yyyy-MM-dd"
 * <li>String {@link LocalDateTime} like "yyyy-MM-dd<b>T</b>hh:mm:ss"
 * <li>String like "yyyy-MM-dd HH:mm:ss"
 * <li><code>Array[int]</code> like [yyyy,mm,dd,hh,MM,ss,ms]
 * </ul>
 * 
 * @author Tritux
 * 
 */
public class LocalDateTimeDeserializer extends StdScalarDeserializer<LocalDateTime> {

	/** {@link long} serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Le but de choisir "02 FEV 1970" est de definir la date de bascule à partir de laquelle on il faut consederer le
	 * timesStamp en seconde et non en milliSeconde
	 */
	private static final Long DEUX_FEVRIER_1970_TIMESSTAMP = LocalDateTime.parse("1970-02-02").toDateTime().getMillis();

	/** Constructor. */
	public LocalDateTimeDeserializer() {
		super(LocalDateTime.class);
	}

	@Override
	public LocalDateTime deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException {

		if (isNullOrEmpty(jp.getText())) {
			return null;
		}
		switch (jp.getCurrentToken()) {
		case START_ARRAY:
			return forStartArray(jp, ctxt);
		case VALUE_NUMBER_INT:
			Long timesStamp = jp.getLongValue();
			// verifier si le timestamp doit être traiter comme un seconde TimesStamp ou un milliSeconde TimesStamp
			timesStamp = timesStamp > DEUX_FEVRIER_1970_TIMESSTAMP ? timesStamp : SECONDS.toMillis(timesStamp);
			return new LocalDateTime(timesStamp);
		case VALUE_STRING: {
			try {
				return LocalDateTime.parse(jp.getText());

			} catch (Exception e) {
				try {
					return new LocalDateTime(Long.valueOf(jp.getText().trim()));

				} catch (Exception exp) {
					return new LocalDateTime(jp.getText().replace(" ", "T").trim());
				}
			}
		}
		default:
			throw ctxt.wrongTokenException(jp, JsonToken.START_ARRAY, "expected JSON Array, Number or String");
		}

	}

	/**
	 * forStartArray.
	 * 
	 * @param jp
	 *            {@link JsonParser}
	 * @param ctxt
	 *            {@link DeserializationContext}
	 * @return the deserialized {@link LocalDateTime}
	 * @throws IOException
	 *             if error occurs
	 */
	private static LocalDateTime forStartArray(final JsonParser jp, final DeserializationContext ctxt)
			throws IOException {
		// [yyyy,mm,dd,hh,MM,ss,ms]
		if (jp.isExpectedStartArrayToken()) {
			jp.nextToken(); // VALUE_NUMBER_INT
			int year = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT
			int month = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT
			int day = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT
			int hour = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT
			int minute = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT
			int second = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT | END_ARRAY
			// let's leave milliseconds optional?
			int millisecond = 0;
			if (jp.getCurrentToken() != JsonToken.END_ARRAY) { // VALUE_NUMBER_INT
				millisecond = jp.getIntValue();
				jp.nextToken(); // END_ARRAY?
			}
			if (jp.getCurrentToken() != JsonToken.END_ARRAY) {
				throw ctxt.wrongTokenException(jp, JsonToken.END_ARRAY, "after LocalDateTime ints");
			}
			return new LocalDateTime(year, month, day, hour, minute, second, millisecond);
		}
		throw ctxt.wrongTokenException(jp, JsonToken.START_ARRAY, "expected JSON Array, Number or String");
	}
}
