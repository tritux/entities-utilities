package com.tritux.common.date;


import static com.tritux.common.utils.Null.isNullOrEmpty;

import java.io.IOException;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;

/**
 * Json Deserializer for {@link LocalDate}.
 *
 * @author Tritux
 *
 */
public class LocalDateDeserializer extends StdScalarDeserializer<LocalDate> {

	/** {@link long} serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Constructor. */
	public LocalDateDeserializer() {
		super(LocalDate.class);
	}

	@Override
	public LocalDate deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException {
		if (isNullOrEmpty(jp.getText())) {
			return null;
		}
		try {
			return LocalDate.parse(jp.getText());
		} catch (Exception e) {
			try {
				// check if the date is a loclDateTime
				return LocalDateTime.parse(jp.getText()).toLocalDate();
			} catch (Exception e2) {
				switch (jp.getCurrentToken()) {
					case START_ARRAY:
						return forStartArray(jp, ctxt);
					case VALUE_NUMBER_INT:
						return new LocalDate(jp.getLongValue());
					default:
						throw ctxt.wrongTokenException(jp, JsonToken.START_ARRAY,
								"expected JSON Array, String or Number");
				}
			}
		}
	}

	/**
	 * forStartArray.
	 *
	 * @param jp
	 *            {@link JsonParser}
	 * @param ctxt
	 *            {@link DeserializationContext}
	 * @return the deserialized {@link LocalDate}
	 * @throws IOException
	 *             if error occurs
	 */
	private static LocalDate forStartArray(final JsonParser jp, final DeserializationContext ctxt) throws IOException {
		// [yyyy,mm,dd]
		if (jp.isExpectedStartArrayToken()) {
			jp.nextToken(); // VALUE_NUMBER_INT
			int year = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT
			int month = jp.getIntValue();
			jp.nextToken(); // VALUE_NUMBER_INT
			int day = jp.getIntValue();
			if (jp.nextToken() != JsonToken.END_ARRAY) {
				throw ctxt.wrongTokenException(jp, JsonToken.END_ARRAY, "after LocalDate ints");
			}
			return new LocalDate(year, month, day);
		}
		throw ctxt.wrongTokenException(jp, JsonToken.START_ARRAY, "expected JSON Array, String or Number");
	}
}
