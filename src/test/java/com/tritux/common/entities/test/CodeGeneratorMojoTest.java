package com.tritux.common.entities.test;

import org.junit.Ignore;

import com.tritux.common.entities.CodeGeneratorMojo;


/**
 * CodeGeneratorMojoTest.
 *
 * @author Tritux
 *
 */
@Ignore
public class CodeGeneratorMojoTest {

	/**
	 * main.
	 *
	 * @param args
	 *            args
	 */
	public static void main(final String... args) {
		new CodeGeneratorMojoTest();
	}

	/**
	 * Constructor.
	 *
	 */
	protected CodeGeneratorMojoTest() {
		try {
			CodeGeneratorMojo codeGeneratorMojo = new CodeGeneratorMojo();
			codeGeneratorMojo.setFile("./entities.xml");
			codeGeneratorMojo.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
